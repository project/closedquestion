<?php

/**
 * @file
 * Storage for our XML templates.
 */

/**
 * Defines the actual XML templates. Stored in a separate file to avoid being
 * processed on every page load.
 *
 * @see closedquestion_closedquestion_templates()
 */
function _closedquestion_closedquestion_templates() {
  return [
    'cq_arrow' => [
      'name' => 'Arrow question',
      'xml' => '<question type="arrow" linenumbering="yes" linestyle="curved" startarrow="no" endarrow="yes" maxchoices="2">
  <text>
    <div>Drag some arrows</div>
    <ul class="help">
      <li>Click and drag to draw an arrow from one hotspot (gray rectangle) to another</li>
      <li>press Ctrl while drawing an arrow to flip it</li>
    </ul>
  </text>
  <matchimg src="">
    <hotspot id="a" shape="rect" coords="167,100,192,120"></hotspot>
    <hotspot id="b" shape="rect" coords="151,166,170,192"></hotspot>
    <hotspot id="c" shape="rect" coords="132,227,100,201"></hotspot>
  </matchimg>
  <hint mintries="1">A first hint.</hint>
  <hint mintries="2">A second hint.</hint>
  <hint mintries="3">A third hint.</hint>
  <mapping correct="0" stop="1">
    <match pattern="^$"/>
    <feedback>You forgot to answer!</feedback>
  </mapping>
  <mapping correct="0">
    <match pattern="ba" hotspot="Enter text" />
    <feedback>Not correct, try again.</feedback>
  </mapping>
  <mapping correct="1" stop="1">
    <match pattern="^ab,bc$" />
    <feedback>Correct!</feedback>
  </mapping>
</question>',
    ],
    'cq_multiple_choice' => [
      'name' => 'Multiple choice',
      'xml' => '<question type="OPTION">
  <text>Put the question here</text>
  <prompt>Pick one</prompt>
  <hint mintries="1">A first hint.</hint>
  <hint mintries="2">A second hint.</hint>
  <hint mintries="3">A third hint.</hint>
  <option correct="0">
    <choice>Option 1</choice>
    <feedback>feedback for option 1</feedback>
  </option>
  <option correct="0">
    <choice>Option 2</choice>
    <feedback>feedback for option 2</feedback>
  </option>
  <option correct="1">
    <choice>Option 3</choice>
    <feedback>feedback for option 3</feedback>
  </option>
  <option correct="0">
    <choice>Option 4</choice>
    <feedback>feedback for option 4</feedback>
  </option>
</question>',
    ],
    /*    'cq_multiple_answer' => array(
      'name' => 'Multiple answer: simple feedback',
      'xml' => '<question type="check">
      <text>Put the question here</text>
      <prompt>Choose one or more options:</prompt>
      <correct>Feedback if the student answers the question correctly.</correct>
      <hint mintries="1">A first hint.</hint>
      <hint mintries="2">A second hint.</hint>
      <hint mintries="3">A third hint.</hint>
      <option>
      <choice>Option 1</choice>
      <feedback>feedback for option 1</feedback>
      </option>
      <option correct="1">
      <choice>Option 2</choice>
      <feedback>feedback for option 2</feedback>
      </option>
      <option correct="1">
      <choice>Option 3</choice>
      <feedback>feedback for option 3</feedback>
      </option>
      <option>
      <choice>Option 4</choice>
      <feedback>feedback for option 4</feedback>
      </option>
      </question>'
      ), */
    'cq_multiple_answer' => [
      'name' => 'Multiple answer',
      'xml' => '<question type="check"><text>Put the question here</text><prompt>Choose one or more options:</prompt><hint mintries="1">A first hint.</hint><hint mintries="2">A second hint.</hint><hint mintries="3">A third hint.</hint><option id="a"><choice>Option 1</choice></option><option id="b"><choice>Option 2</choice></option><option id="c"><choice>Option 3</choice></option><option id="d"><choice>Option 4</choice></option><mapping stop="1"><feedback>You forgot to answer!</feedback><match pattern="^$"/></mapping><mapping correct="1"><match pattern="^(?=.*a).{1}$"/><feedback>That is correct.</feedback></mapping></question>',
    ],
    'cq_select' => [
      'name' => 'Select',
      'xml' => '<question type="selectorder" onlyorder="2">
        <text>Select the required materials for both experiments.</text>
        <option id="a"><choice>Material A.</choice></option>
        <option id="b"><choice>Material B.</choice></option>
        <option id="c"><choice>Material C.</choice></option>
        <option id="d"><choice>Material D.</choice></option>
        <option id="e"><choice>Material E.</choice></option>
        <option id="f"><choice>Material F.</choice></option>
        <target id="1"><choice>Experiment 1</choice></target>
        <target id="2"><choice>Experiment 2</choice></target>
        <hint mintries="1">A first hint.</hint>
        <hint mintries="2">A second hint.</hint>
        <hint mintries="3">A third hint.</hint>
        <mapping correct="1"><match pattern="^((cbd)|(bcd)|(dcb)|(cdb)|(bdc)|(dbc))$"/>
          <feedback>Correct. You only need materials B, C and D for Experiment 1</feedback>
        </mapping>
        <mapping stop="1">
          <feedback>Make sure you drag at least one answer option to each answer field!</feedback>
            <or>
              <match pattern="^(1)(?=[a-zA-Z]{0}2)(2)(.*)$"/>
              <match pattern="^(1)(.*)(2)(?=[a-zA-Z]{0}$)$"/>
            </or>
        </mapping>
        <mapping correct="0">
          <feedback>You do need material A or F for Experiment 2 </feedback>
          <or>
            <match pattern="^(1)(?=[a-zA-Z]{0}2)(2)(?=[a-zA-Z]{1}$)(?=[^a]*a{1})(.*)$" />
            <match pattern="^(1)(?=[a-zA-Z]{0}2)(2)(?=[a-zA-Z]{1}$)(?=[^f]*f{1})(.*)$" />
          </or>
        </mapping>
      </question>',
    ],
    'cq_select_and_order' => [
      'name' => 'Select & order',
      'xml' => ' <question type="selectorder">
  <text>Select and order the needed steps. This example question contains two target boxes.</text>
  <option id="a"><choice>Step A.</choice></option >
  <option id="b"><choice>Step B.</choice></option >
  <option id="c"><choice>Step C.</choice></option >
  <option id="d"><choice>Step D.</choice></option >
  <option id="e"><choice>Step E.</choice></option >
  <option id="f"><choice>Step F.</choice></option >
  <target id="1"><choice>Day 1 </choice></target >
  <target id="2"><choice>Day 2 </choice></target >
  <hint mintries="1">A first hint.</hint>
  <hint mintries="2">A second hint.</hint>
  <hint mintries="3">A third hint.</hint>
  <mapping correct="0" stop="1">
    <match pattern="^$" />
    <feedback>You forgot to answer!</feedback>
  </mapping>
  <mapping correct="1">
    <match pattern="^1bc2d$" />
    <feedback>Correct. You need to do steps B and C on day 1 and step D on day 2. </feedback>
  </mapping>
  <mapping correct="0">
    <feedback>You can not do step C unless you first do step B.</feedback>
    <or>
      <match pattern="^1.*cb.*2.*$" />
      <match pattern="^1.*2.*cb.*$" />
    </or>
  </mapping>
  <mapping correct="0" stop="1">
    <feedback>Make sure you drag at least one answer option to each answer field!</feedback>
    <or>
      <match pattern="^12.*$"/>
      <match pattern="^1.*2$"/>
    </or>
  </mapping>
</question> ',
    ],
    'cq_order' => [
      'name' => 'Order',
      'xml' => ' <question type="selectorder" duplicates="0" onlyorder="1">
  <text>Order the needed steps.</text>
  <option id="a">
    <choice>Step A.</choice>
  </option >
  <option id="b">
    <choice>Step B.</choice>
  </option >
  <option id="c">
    <choice>Step C.</choice>
  </option >
  <option id="d">
    <choice>Step D.</choice>
  </option >
  <option id="e">
    <choice>Step E.</choice>
  </option >
  <option id="f">
    <choice>Step F.</choice>
  </option >
    <hint mintries="1">A first hint.</hint>
    <hint mintries="2">A second hint.</hint>
    <hint mintries="3">A third hint.</hint>
  <mapping correct="0" stop="1">
    <match pattern="^$" />
    <feedback>You forgot to answer!</feedback>
  </mapping>
  <mapping correct="1">
    <match pattern="^abcdef$" />
    <feedback>Yes!</feedback>
  </mapping>
    <mapping correct="0">
    <match pattern="^[^b]*c.*$" />
    <feedback>You can not do step C unless you first do step B.</feedback>
  </mapping>
</question> ',
    ],
    'cq_hotspot' => [
      'name' => 'Hotspot / point and click',
      'xml' => ' <question type="hotspot" maxchoices="2">
  <text><div>Point out the correct two items.</div ></text>
  <matchimg src="">
    <hotspot id="a" shape="circle" coords="275,68,68"></hotspot >
    <hotspot id="b" shape="rect" coords="0,21,43,121"></hotspot >
    <hotspot id="c" shape="poly" coords="174,282,258,293,320,318,366,360,267,468,207,438,149,443,99,465,85,486,7,416,7,354,64,307"></hotspot >
  </matchimg >
   <hint mintries="1">A first hint.</hint>
   <hint mintries="2">A second hint.</hint>
   <hint mintries="3">A third hint.</hint>
  <mapping correct="1">
    <match hotspot="a" />
    <match hotspot="b" />
    <feedback>Very Good, you pointed out the correct two items </feedback>
  </mapping>
  <mapping correct="0">
    <match hotspot="c" />
    <feedback>You have pointed out an incorrect item.</feedback>
  </mapping>
  <mapping correct="0">
    <not ><match hotspot="a" /></not >
    <feedback>You have not pointed out the first correct item.</feedback>
  </mapping>
  <mapping stop="1">
    <feedback>Please answer the question first!</feedback>
    <not>
      <match draggable=".*" hotspot=".*"/>
    </not>
  </mapping>
</question> ',
    ],
    'cq_drag_and_drop' => [
      'name' => 'Drag & drop',
      'xml' => ' <question type="dragdrop">
  <text><div>Drag the items to the corresponding locations on the image.</div ></text>
  <matchimg >
    <hotspot shape="rect" coords="294,43,447,186" id="a1" />
    <hotspot shape="circle" coords="222,370,34" id="a2" />
    <hotspot shape="poly" coords="344,573,340,689,505,691,498,588" id="a3" />
    <draggable id="d1">Draggable 1 </draggable >
    <draggable id="d2">Draggable 2 </draggable >
    <draggable id="d3">Draggable 3 </draggable >
  </matchimg >
  <startstate value="d1,387,103;d2,393,353;d3,631,65;" />
  <mapping correct="1">
    <feedback>Correct. You have put the items at the right locations.</feedback>
    <match hotspot="a1" draggable="d1" />
    <match hotspot="a2" draggable="d2" />
    <match hotspot="a3" draggable="d3" />
  </mapping>
  <mapping stop="1">
    <match/>
    <feedback>Please answer the question.</feedback>
  </mapping>
</question> ',
    ],
    'cq_fill_in_the_blanks_textfield' => [
      'name' => 'Fill in the blanks: text field',
      'xml' => ' <question type="fillblanks">
  <text><div>You have measured a concentration of 10 g / l in a 10 - times diluted sample.<br />
What was the concentration in the non - diluted solution ?</div >
<div ><b>The original concentration was: <inlinechoice id="c" freeform="1" />g / l</b ></div ></text>
  <hint mintries="1">A first hint.</hint>
  <hint mintries="2">A second hint.</hint>
  <hint mintries="3">A third hint.</hint>
  <mapping correct="1">
    <range inlinechoice="c" minval="100" maxval="100" />
    <feedback>Very good!</feedback>
  </mapping>
  <mapping correct="0" stop="1">
    <match inlinechoice="c" pattern="^$" />
    <feedback>You forgot to answer!</feedback>
  </mapping>
  <mapping correct="0" stop="1">
    <not ><match inlinechoice="c" pattern="^[+-]?[0-9]+([,.][0-9]+)?([eE][+-]?[0-9]+)?$" /></not >
    <feedback>Please give a number.</feedback>
  </mapping>
  <mapping correct="0" stop="1">
    <match inlinechoice="c" pattern="^[-+]?\d*\,\d*$" />
    <feedback>Use a . as decimal separator instead of a comma.</feedback>
  </mapping>
</question> ',
    ],
    'cq_fill_in_the_blanks_options' => [
      'name' => 'Fill in the blanks: options',
      'xml' => ' <question type="fillblanks">
	<text>
		<div>Fill the gaps:</div >
		<div>First you should do <inlinechoice id="a" freeform="0" />,
then <inlinechoice id="b" freeform="0" />.</div >
	</text>
	<hint mintries="1">A first hint.</hint>
	<hint mintries="2">A second hint.</hint>
	<hint mintries="3">A third hint.</hint>
	<mapping correct="1">
		<feedback>Very good!</feedback>
		<and>
			<match inlinechoice="a" inlineoption="a" />
			<match inlinechoice="b" inlineoption="b" />
		</and>
	</mapping>
	<mapping correct="0" stop="1">
		<feedback>Fill in all the gaps!</feedback>
		<or>
			<match inlinechoice="a" inlineoption="x" />
			<match inlinechoice="b" inlineoption="x" />
		</or>
	</mapping>
	<inlineoption id="x">...</inlineoption>
	<inlineoption id="a">Option A </inlineoption>
	<inlineoption id="b">Option B </inlineoption>
	<inlineoption id="c">Option C </inlineoption>
</question> ',
    ],
    'cq_fill_in_the_blanks_math' => [
      'name' => 'Fill in the blanks: advanced math',
      'xml' => ' <question type="fillblanks">
  <matheval e="a=random()" store="1" />
  <matheval e="b=random()" store="1" />
  <matheval e="a=round(a*0.8+0.11,2)" />
  <matheval e="b=round(b*5+2,0)" />
  <matheval e="ans=a*b" />
  <matheval e="min=ans-0.01" />
  <matheval e="max=ans+0.01" />
  <text><div>You have measured a concentration of <mathresult e="a" />mg / ml in a <mathresult e="b" />-times diluted sample.<br />
What was the concentration in the non - diluted solution ?</div >
<div ><b>The original concentration was: <inlinechoice id="c" freeform="1" />g / l</b ></div ></text>
  <hint mintries="1">A first hint.</hint>
  <hint mintries="2">A second hint.</hint>
  <hint mintries="3">A third hint.</hint>
  <mapping stop="1">
    <feedback>You forgot to answer!</feedback>
    <match inlinechoice="c" pattern="^$" />
  </mapping>
  <mapping correct="1">
    <range inlinechoice="c" minval="min" maxval="max" />
    <feedback>Very good. The difference between your answer and the "real" answer is: <mathresult e="abs(ans-c)" /></feedback>
  </mapping>
  <mapping correct="0" stop="1">
    <not ><match inlinechoice="c" pattern="^[+-]?[0-9]+([,.][0-9]+)?([eE][+-]?[0-9]+)?$" /></not >
    <feedback>Please give a number.</feedback>
  </mapping>
</question> ',
    ],
    'cq_open' => [
      'name' => 'Open question',
      'xml' => '<question type="fillblanks"><text><div>Open question?</div>
<div><inlinechoice id="student_answer" freeform="1" longtext="1"/>
</div>
<div>Teacher feedback<br />
<inlinechoice id="teacher_feedback" freeform="1" longtext="1"/>
</div>
<div>Did you read the teacher feedback?</div>
<div>
  <inlinechoice group="yes_no" freeform="0" id="feedback_read"/>
</div></text><comment text="Check if student has read answer"/><mapping><feedback>Please answer the question!<br /></feedback><match inlinechoice="student_answer" pattern="^$"/></mapping><comment text="Student answered, the teacher should give feedback"/><mapping correct="1"><feedback>Your answer has been stored. The teacher will give feedback.<br /></feedback><and><not><match inlinechoice="student_answer" pattern="^$"/></not><match inlinechoice="teacher_feedback" pattern="^$"/></and></mapping><comment text="The teacher gave feedback, the student should read it."/><mapping><feedback>The teacher gave feedback. Please read it!<br /></feedback><and><not><match inlinechoice="student_answer" pattern="^$"/><match inlinechoice="teacher_feedback" pattern="^$"/></not><match inlinechoice="feedback_read" inlineoption="no"/></and></mapping><comment text="The student read the feedback and finished the question"/><mapping correct="1"><feedback>Great job!<br /></feedback><and><not><match inlinechoice="student_answer" pattern="^$"/><match inlinechoice="teacher_feedback" pattern="^$"/></not><match inlinechoice="feedback_read" inlineoption="yes"/></and></mapping><inlineoption group="yes_no" id="no">No</inlineoption><inlineoption group="yes_no" id="yes">Yes</inlineoption></question>',
    ],
  ];
}
