<?php

/**
 * @file
 * CqUserAnswerDefault is the default implementation of CqUserAnswerInterface.
 *
 * It stores data for authenticated users in the database and data for anonymous
 * users in the session of the user.
 */
class CqUserAnswerDefault implements CqUserAnswerInterface {

  /**
   * The answer set by the user.
   *
   * @var mixed
   */
  private $answer;

  /**
   * The answer entity object.
   *
   * @var entity
   */
  private $answer_entity;

  /**
   * Additional data, as key/value pairs.
   *
   * @var array
   */
  private $data;

  /**
   * The number of times the student gave a wrong answer.
   *
   * @var int
   */
  private $tries = 0;

  /**
   * Is the current answer the correct answer?
   * 1: yes
   * 0: no
   * -1: correctness not determined yet.
   *
   * @var int
   */
  private $isCorrect = -1;

  /**
   * Has the student ever answered this question correct, even though his
   * current answer might be incorrect?
   *
   * @var int
   */
  private $onceCorrect = 0;

  /**
   * The serialised version of the previous answer. Used to see if anything
   * changed.
   *
   * @var string
   */
  private $origAnswerSerialised;

  /**
   * The serialised version of the previous data. Used to see if anything
   * changed.
   *
   * @var string
   */
  private $origDataSerialised;

  /**
   * The previous version of the tries parameter. Used to see if anything
   * changed.
   *
   * @var int
   */
  private $origTries;

  /**
   * The previous version of the onceCorrect parameter. Used to see if anything
   * changed.
   *
   * @var int
   */
  private $origCorrect;

  /**
   * The node id of the Drupal node this UserAnswer belongs to.
   *
   * @var int
   */
  private $nodeId;

  /**
   * The user id of the Drupal user.
   *
   * @var int
   */
  private $userId;

  /**
   * Is there already an answer in the database? If so, we need to UPDATE and
   * not INSERT. Default=FALSE
   *
   * @var boolean
   */
  private $inDatabase = FALSE;

  /**
   *  The aid of the stored answer entity, if answer is loaded from database.
   * @var int
   */
  private $aid = 0;

  /**
   *  The context id this answer belongs to.
   *
   * @var string
   */
  private $context_id = FALSE;

  /**
   * The question object for this answer object.
   *
   * @var object
   */
  private $question;

  /**
   * Constructs a new CqUserAnswerDefault
   *
   * @param int $nid
   *   The node id this answer belongs to.
   * @param int $uid
   *   The user id of the user this answer belongs to.
   * @param string $context_id
   */
  public function __construct($nid, $uid, $context_id = '') {

    $this->nodeId = (int) $nid;
    $this->userId = (int) $uid;

    // Use hook to allow other modules to set or change the context_id.
    // Default implementation in closedquestion.module (get from URL parameter).
    drupal_alter('closedquestion_context_id', $context_id, $nid, $uid);
    if (!empty($context_id)) {
      $this->setContextId($context_id);
    }
    $row = FALSE;

    if ($this->userId > 0 && $this->nodeId > 0) {
      $answer_entity = $this->fetchFromDatabase();
      if (!empty($answer_entity)) {
        $this->answer_entity = $answer_entity;
        $this->origAnswerSerialised = $answer_entity->answer;
        $this->origDataSerialised = $answer_entity->data;
        $this->onceCorrect = $answer_entity->once_correct;
        $this->origCorrect = $answer_entity->once_correct;
        $this->origTries = $answer_entity->tries;
        $this->answer = unserialize($this->origAnswerSerialised);
        $this->data = unserialize($this->origDataSerialised);
        $this->tries = $this->origTries;
        $this->inDatabase = TRUE;
        $this->aid = $answer_entity->aid;
        $field_context_id_ = field_get_items('cq_user_answer_entity', $answer_entity, 'cq_context_id');
        if (!empty($field_context_id_) && isset($field_context_id_[0]['value'])) {
          $this->setContextId($field_context_id_[0]['value']);
        }
      }
    }
    elseif ($this->nodeId > 0) {
      if (isset($_SESSION['cq']['answers'][$nid]) && variable_get('closedquestion_anonymous_user_save_answer')) {
        $row = $_SESSION['cq']['answers'][$nid];
        $this->origAnswerSerialised = $row['answer'];
        $this->origDataSerialised = $row['data'];
        $this->onceCorrect = $row['once_correct'];
        $this->origCorrect = $row['once_correct'];
        $this->origTries = $row['tries'];
        $this->answer = unserialize($this->origAnswerSerialised);
        $this->data = unserialize($this->origDataSerialised);
        $this->tries = $this->origTries;
      }
    }
  }

  /**
   * Return stored data from user answer entity as array.
   *
   * This represents the data from the database.
   *
   * @return array
   */
  public function getAnswerDataAsArray() {
    $answer_data = array();
    if (!empty($this->answer_entity)) {
      $answer_wrapper = entity_metadata_wrapper('cq_user_answer_entity', $this->answer_entity);
      if (!empty($answer_wrapper)) {
        $props = $answer_wrapper->getPropertyInfo();
        // Rename these fields for backward compatibility.
        $rename_fields = array(
          'cq_context_id' => 'context_id',
          'cq_feedback' => 'feedback'
        );
        // Ignore these properties for backward compatibility.
        $ignore_fields = array('url', 'module', 'status', 'label', 'name');
        foreach ($props AS $field_name => $field) {
          $fid = $field_name;
          if (array_key_exists($field_name, $rename_fields)) {
            $fid = $rename_fields[$field_name];
          }
          if (!in_array($field_name, $ignore_fields)) {
            $answer_data[$fid] = $answer_wrapper->$field_name->value();
          }
        }
      }
    }
    return $answer_data;
  }

  /**
   * Implements CqUserAnswer::store()
   */
  public function store() {
    // Create answer array
    $answerdata = array(
      'uid' => $this->userId,
      'nid' => $this->nodeId,
      'data' => serialize($this->data),
      'answer' => serialize($this->answer),
      'tries' => $this->tries,
      'once_correct' => $this->onceCorrect,
      'is_correct' => $this->isCorrect,
      'module' => 'closedquestion',
      'name' => 'cq_user_answer_entity',
      'label' => entity_get_info('cq_user_answer_entity')['label'],
      'unixtime' => REQUEST_TIME,
    );
    // Check if answer has changed
    if ($answerdata['data'] != $this->origDataSerialised ||
      $answerdata['answer'] != $this->origAnswerSerialised ||
      $this->origTries != $this->tries ||
      $this->origCorrect != $this->onceCorrect) {

      // Create or load entity.
      // Pass entity to hook.
      // Other modules can change entity.
      // Save updated entity.
      // Save copy of entity to log table.
      //
      // ISSUES:
      //  1) nid and uid are no longer valid as unique key for an answer entity.
      //  hook_cq_save_answer can add extra context data to the entity that can
      //  also be added in hook_cq_load_answer_conditions to find the answer.
      //  We dont know the complete context, until an answer is submitted.
      //  FIXED: by setting context id in constructor.
      //  2) CqUserAnswerDefault objects are being cached using nid and
      //  uid as keys (see function &closedquestion_get_useranswer($nid, $uid)
      //  in FactoryQuestion.inc.php, but this is no longer a valid unique key.
      //  FIX: don't use closedquestion_get_useranswer to creat a CqUserAnswer
      //  object.
      //  NOTE: This will possibly break math export and import for
      //  closedquestion answers with a context id.
      //  3) store() method can be called before saving an answer when a matheval
      //  expression with a store setting is used in the question. Problem is
      //  that we don't know the context_id yet, because it is determined when the
      //  answer is submitted.
      //  FIXED by setting context_id in constructor.
      //
      // Let other modules change the answerdata: use standard entity hooks.
      // Store the answer

      $context_id = $this->getContextId();
      if (empty($context_id)) {
        // In case answer is stored before it is submitted by the user
        // (e.g. when a matheval expression with a store setting is used in the
        // question), try to get contextId from URL.
        // TODO: Should be fixed now by setting context_id in constructor.
        //       This fallback can probably be removed now (needs more testing).
        $context_id = filter_input(INPUT_GET, "contextId", FILTER_SANITIZE_STRING);
      }
      // Get feedback text.
      $feedback_text = '';
      if (isset($this->question)) {
        $feedback_items = $this->question->getFeedbackItems();
        foreach ($feedback_items as $fb) {
          $feedback_text = $feedback_text . $fb->getText();
        }
      }
      if ($answerdata['uid'] > 0 && $answerdata['nid'] > 0) {
        if ($this->aid > 0) {
          if (!empty($this->answer_entity)) {
            $answer_wrapper = entity_metadata_wrapper('cq_user_answer_entity', $this->answer_entity);
          }
          else {
            $answer_wrapper = entity_metadata_wrapper('cq_user_answer_entity', $this->aid);
          }
          $answer_entity = $answer_wrapper->value();
          foreach ($answerdata as $field => $value) {
            $answer_wrapper->$field->set($value);
          }

          if (!empty($context_id)) {
            // Add context to answer entity.
            $answer_wrapper->cq_context_id->set($context_id);
          }
          if (!empty($feedback_text)) {
            $answer_wrapper->cq_feedback->set($feedback_text);
          }
          // No need for special hook:
          // drupal_alter('cq_save_answer', $answer_entity, $this->nodeId);
          // use hook_cq_user_answer_entity_presave($answer_entity).
          $answer_wrapper->save();
        }
        else {
          $answer_entity = entity_create('cq_user_answer_entity', $answerdata);
          if ($answer_entity !== FALSE) {
            $answer_wrapper = $answer_entity->wrapper();
            // No need for special hook:
            // drupal_alter('cq_save_answer', $answer_entity, $this->nodeId);
            // use hook_cq_user_answer_entity_presave($answer_entity).
            if (!empty($context_id)) {
              // Add context to answer entity.
              $answer_wrapper->cq_context_id->set($context_id);
            }
            if (!empty($feedback_text)) {
              $answer_wrapper->cq_feedback->set($feedback_text);
            }
            $answer_wrapper->save();
            $this->aid = $answer_entity->aid;
            $this->answer_entity = $answer_entity;
          }
        }

        // Insert log record
        // @TODO: use revisions instead of log table??
        // Are revisions deleted when the entity is deleted?
        // (re)set some values for creating a log entry.
        $answerdata['name'] = 'cq_user_answer_log_entity';
        $answerdata['label'] = entity_get_info('cq_user_answer_log_entity')['label'];

        // Create log entity.
        $answer_log_entity = entity_create('cq_user_answer_log_entity', $answerdata);
        $answer_log_entity_wrapper = entity_metadata_wrapper('cq_user_answer_log_entity', $answer_log_entity);

        //$answer_wrapper = entity_metadata_wrapper('cq_user_answer_entity', $answer_entity);
        // Copy properies and fields from answer_entity (could be changed in hook).
        $property_info = $answer_wrapper->getPropertyInfo();
        foreach ($property_info as $key => $val) {
          // Not all properties should be copied.
          // @TODO: what is the evalId property, not used in closedquestion?
          if (!in_array($key, array('url', 'name', 'label', 'log_id', 'aid'))) {
            if (isset($answer_log_entity_wrapper->$key)) {
//              if (isset($val['field'])) { // Field.
              $value = $answer_wrapper->$key->value();
              if (!empty($value)) {
                $answer_log_entity_wrapper->$key->set($value);
              }
//              }
//              else { // Property.
//                // Some values are readonly in wrapper, setters not defined?
//                // @TODO: Set correct property field info for entity.
//                $answer_log_entity->$key = $answer_wrapper->$key->value();
//              }
            }
          }
        }

        $answer_log_entity_wrapper->save();
      }
      elseif ($this->nodeId > 0 && variable_get('closedquestion_anonymous_user_save_answer')) {
        $row = array(
          'answer' => $answerdata['answer'],
          'data' => $answerdata['data'],
          'is_correct' => $answerdata['is_correct'],
          'once_correct' => $answerdata['once_correct'],
          'tries' => $answerdata['tries']
        );
        $_SESSION['cq']['answers'][$this->nodeId] = $row;
      }
      $this->origAnswerSerialised = $answerdata['answer'];
      $this->origDataSerialised = $answerdata['data'];
      $this->origTries = $answerdata['tries'];
      $this->origCorrect = $answerdata['once_correct'];
    }
  }

  /**
   * Fetch the answer for this UserAnswer from the database.
   *
   * @return mixed
   *   An array containing the db results or FALSE when there are no results
   */
  private function fetchFromDatabase() {

    $answer = FALSE;
    $answer_entity = FALSE;
    $aid = FALSE;

    // Answer entity query
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'cq_user_answer_entity');
    $query->propertyCondition('nid', $this->nodeId);
    $query->propertyCondition('uid', $this->userId);
    // $query->propertyCondition('module', MODULE_NAME);
    $query->propertyOrderBy('unixtime', 'DESC'); // we want the last answer
    $query->range(0, 1); // we want the last answer
    $context_field_info = field_info_instance('cq_user_answer_entity', 'cq_context_id', 'cq_user_answer_entity');
    if (!empty($context_field_info)) {
      $context_id = $this->getContextId();
      if (!empty($context_id)) {
        $query->fieldCondition('cq_context_id', 'value', $context_id);
        // Add context_id as query metadata.
        $query->addMetaData('context_id', $context_id);
      }
      else {
        // Add this tag to select answers without context id.
        // @TODO: add default context_id for closedquestion without context?
        $query->addTag('cq_empty_context');
      }
    }
    // Hook to allow other modules to alter EntityFieldQuery.
    drupal_alter('cq_load_answer_conditions', $query);

    $result = $query->execute();

    if (isset($result['cq_user_answer_entity'])) {
      $aid = key($result['cq_user_answer_entity']);
      if ($aid) {
        $answer = entity_load('cq_user_answer_entity', array($aid));
        $answer_entity = $answer[$aid];
        // No need for secial hook.
        // drupal_alter('cq_load_answer', $answer_entity);
        // Use hook_cq_user_answer_entity_load($entities).
      }
    }

    return $answer_entity;
  }

  /**
   * Implements CqUserAnser::getAnser()
   */
  public function getAnswer() {
    return $this->answer;
  }

  /**
   * Implements CqUserAnser::setAnswer()
   */
  public function setAnswer($newAnswer) {
    $this->answer = $newAnswer;
  }

  /**
   * Implements CqUserAnser::reset()
   */
  public function reset() {
    $doReset = true;
    $answerdata = array(
      'uid' => $this->userId,
      'nid' => $this->nodeId,
      'data' => $this->data,
      'module' => 'closedquestion',
      'name' => 'cq_user_answer_entity',
      'label' => entity_get_info('cq_user_answer_entity')['label'],
    );
    // Let other modules change the search criteria.
    drupal_alter('cq_reset_answer', $answerdata);

    // Reset the answer.
    if ($doReset !== false) {

      if ($answerdata['uid'] > 0 && $answerdata['nid'] > 0) {
        if ($this->aid > 0) {
          $aid = $this->aid;
        }
        else {
          // Reset called for question without an answer.
          watchdog('closedquestion', 'Reset answer called, without aid value, no answer found.', array(), WATCHDOG_NOTICE);
        }
        if (!empty($aid) && is_numeric($aid)) {
          entity_delete('cq_user_answer_entity', (int) $aid);
        }

        $answerdata['answer'] = 'Reset';
        $answerdata['once_correct'] = 0;
        $answerdata['tries'] = 0;
        $answerdata['is_correct'] = 0;
        $answerdata['unixtime'] = REQUEST_TIME;
        $answerdata['name'] = 'cq_user_answer_log_entity';
        $answerdata['label'] = entity_get_info('cq_user_answer_log_entity')['label'];

        $entity = entity_create('cq_user_answer_log_entity', $answerdata);
        $wrapper = $entity->wrapper();
        $context_id = $this->getContextId();
        if (!empty($context_id)) {
          $wrapper->cq_context_id->set($context_id);
        }
        $wrapper->save();

        $this->answer = NULL;
        $this->tries = 0;
        $this->isCorrect = -1;
        $this->onceCorrect = 0;
        $this->origDataSerialised = '';
        $this->origTries = -1;
        $this->inDatabase = FALSE;
        $this->aid = 0;
      }
      elseif ($answerdata['nid'] > 0) {
        if (isset($_SESSION['cq']['answers'][$answerdata['nid']])) {
          unset($_SESSION['cq']['answers'][$answerdata['nid']]);
        }
      }
    }
  }

  /**
   * Implements CqUserAnser::answerHasChanged()
   */
  public function answerHasChanged() {
    $seranswer = serialize($this->answer);
    return ($seranswer != $this->origAnswerSerialised);
  }

  /**
   * Implements CqUserAnser::getTries()
   */
  public function getTries() {
    return $this->tries;
  }

  /**
   * Implements CqUserAnser::setTries()
   */
  public function setTries($tries) {
    $this->tries = $tries;
  }

  /**
   * Implements CqUserAnser::increaseTries()
   */
  public function increaseTries() {
    $this->tries++;
  }

  /**
   * Implements CqUserAnser::isCorrect()
   */
  public function isCorrect() {
    return $this->isCorrect;
  }

  /**
   * Implements CqUserAnser::isEmpty()
   */
  public function isEmpty() {
    return drupal_strlen((string) $this->answer) <= 0;
  }

  /**
   * Implements CqUserAnser::onceCorrect()
   */
  public function onceCorrect() {
    return $this->onceCorrect;
  }

  /**
   * Implements CqUserAnser::setCorrect()
   */
  public function setCorrect($correct) {
    $this->isCorrect = (int) $correct;
    if ($correct && !$this->onceCorrect) {
      $this->onceCorrect = $this->tries + 1;
    }
  }

  /**
   * Implements CqUserAnser::getCorrect()
   */
  public function getCorrect() {
    return $this->isCorrect;
  }

  /**
   * Implements CqUserAnser::getData()
   */
  public function getData($key) {
    if ($this->data !== NULL && array_key_exists($key, $this->data)) {
      return $this->data[$key];
    }
    return NULL;
  }

  /**
   * Implements CqUserAnser::setData()
   */
  public function setData($key, $value) {
    if ($this->data === NULL) {
      $this->data = array();
    }
    $this->data[$key] = $value;
  }

  /**
   * Implements CqUserAnser::getUserId()
   */
  public function getUserId() {
    return $this->userId;
  }

  /**
   * Get context id for this answer.
   *
   * @return string
   */
  public function getContextId() {
    return $this->context_id;
  }

  /**
   * Set the context id for this answer.
   */
  public function setContextId($value) {
    $this->context_id = $value;
  }

  /**
   * Set the question object for this answer.
   *
   * Need this to get and store the feedback text with the answer.
   * NOTE: this introduces a circular reference between the question and the
   * answer object!
   */
  public function setQuestion($value) {
    $this->question = $value;
  }

}
