<?php

/**
 * @file
 * CqMatchRange checks if one or more inlineChoice(s) contains a numeric value
 * in a certain range, or if a certain given value is in a certain range.
 */
class CqMatchRange extends CqAbstractMapping {

  /**
   * Implements CqAbstractMapping::evaluate()
   */
  function evaluate() {
    $choiceId = $this->getParam('inlinechoice');
    $matchAll = $this->getParam('matchall');
    $minvalParam = $this->getParam('minval');
    $maxvalParam = $this->getParam('maxval');
    $valueParam = $this->getParam('value');

    $value = $this->evaluateVar($valueParam);
    if (is_null($choiceId) && is_null($value)) {
      drupal_set_message(t('Range without inlineChoice or value attribute found.'), 'warning');
    }

    // Get answer.
    if (!is_null($choiceId)) {
      $answer = $this->context->getAnswerForChoice($choiceId);
    }
    else {
      $answer = $value;
    }

    // Process minval and maxval.
    $minval = $this->processVar($minvalParam, $answer);
    $maxval = $this->processVar($maxvalParam, $answer);

    // Try to match.
    if (!is_null($answer)) {
      if (is_array($answer)) {
        $i = 0;
        foreach ($answer as $subChoice => $subAnswer) {
          $this->topParent->lastMatchedId = $subChoice;

          $subAnswer = closedquestion_fix_number($subAnswer);
          $subMinval = is_array($minval) ? $minval[$i] : $minval;
          $subMaxval = is_array($maxval) ? $maxval[$i] : $maxval;

          $matched = TRUE;

          if (!is_numeric($subAnswer)) {
            $matched = FALSE;
          }

          if (!is_null($minval) && $subAnswer < $subMinval) {
            $matched = FALSE;
          }
          if (!is_null($maxval) && $subAnswer > $subMaxval) {
            $matched = FALSE;
          }
          if (!$matchAll && $matched) {
            // This sub-answer mached, and we need only one to match, so this
            // range matches, return TRUE.
            return TRUE;
          }
          if ($matchAll && !$matched) {
            // This sub-answer did not match, and we need all of 'em to match
            // for true, so the match failed, return FALSE.
            return FALSE;
          }

          $i++;
        }
        // If we did not return sooner then the result of the last one is final.
        return $matched;
      }
      else {
        $this->topParent->lastMatchedId = $choiceId;

        $answer = closedquestion_fix_number($answer);

        if (!is_numeric($answer)) {
          return FALSE;
        }

        if (!is_null($minval) && $answer < $minval && (string) $answer !== (string) $minval) { // String check is for failing float comparisons. @TODO: find a better way of doing this.
          return FALSE;
        }
        if (!is_null($maxval) && $answer > $maxval && (string) $answer !== (string) $maxval) {
          return FALSE;
        }
        return TRUE;
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * Tries to math evaluate var.
   *
   * @param mixed $var
   *
   * @return mixed
   */
  private function evaluateVar($var) {
    if (!is_null($var) && !is_numeric($var)) {
      // Non numeric var, probably a math expression.
      $var = $this->context->evaluateMath($var);
    }

    return $var;
  }

  /**
   * Checks if variable contains power match and math evaluates var if needed.
   *
   * @param string $var
   *
   * @param array $answer
   *
   * @return number|array
   *   Array when var contains power match, number otherwise
   */
  private function processVar($var, $answer) {
    $hasPowerMatch = strpos($var, '...') > 0;
    $hasAnswer = is_array($answer);

    if ($hasPowerMatch && $hasAnswer) {
      // We now expect serial numbers (001, 002 etc) in the answer and
      // create a new var for each serial number.
      $answer_keys = array_keys($answer);
      $var_template = $var;
      $var = [];
      $sn_matches = [];

      foreach ($answer_keys as $key) {
        // Find serial number in answer key.
        preg_match_all('/(\d+)$/', $key, $sn_matches);

        // Add serial number to var having '...'.
        $replaced = preg_replace('/\.\.\./', $sn_matches[0][0], $var_template);

        // Evaluate newly created var add it to the stack.
        $var[] = $this->evaluateVar($replaced);
      }
    }
    elseif ($hasPowerMatch && !$hasAnswer) {
      // We cannot replace power match as we don't have an answer: Destroy var.
      $var = NULL;
    }
    else {
      // No powermatch: Just evaluate var.
      $var = $this->evaluateVar($var);
    }

    return $var;
  }

  /**
   * Overrides CqAbstractMapping::getAllText()
   */
  public function getAllText() {
    $choiceId = $this->getParam('inlinechoice');
    $value = $this->getParam('value');

    $minval = $this->getParam('minval');
    if (is_null($minval)) {
      $minval = '-&infin;';
    }
    else {
      $minval = check_plain($minval);
    }

    $maxval = $this->getParam('maxval');
    if (is_null($maxval)) {
      $maxval = '&infin;';
    }
    else {
      $maxval = check_plain($maxval);
    }

    $retval = [];

    if (!is_null($choiceId)) {
      $retval['logic']['#markup'] = t('Range: choiceId=%id, minval=!minval, maxval=!maxval.', [
        '%id' => $choiceId,
        '!minval' => $minval,
        '!maxval' => $maxval,
      ]);
    }
    elseif (!is_null($value)) {
      $retval['logic']['#markup'] = t('Range: value=%id, minval=!minval, maxval=!maxval.', [
        '%id' => $value,
        '!minval' => $minval,
        '!maxval' => $maxval,
      ]);
    }
    else {
      $retval['logic']['#markup'] = t('Range: minval=!minval, maxval=!maxval.', [
        '!minval' => $minval,
        '!maxval' => $maxval,
      ]);
    }

    $retval += parent::getAllText();
    return $retval;
  }

  private function preg_array_key_exists($pattern, $array) {
    return count($this->preg_array_key_matches($pattern, $array)) > 0;
  }

  private function preg_array_key_matches($pattern, $array) {
    $keys = array_keys($array);
    $matches = preg_grep($pattern, $keys);
    return $matches;
  }

}
