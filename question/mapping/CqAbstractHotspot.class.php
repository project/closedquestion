<?php

/**
 * @file
 * A hotspot defines a clickable/selectable area on an image.
 *
 * This is currently used by Drag&Drop and Hotspot questions.
 */

/**
 * Functions as base for all hotspot classes.
 */
abstract class CqAbstractHotspot implements CqHotSpotInterface {

  /**
   * The identifier of the hotspot.
   *
   * @var string
   */
  protected $identifier = 'NoName';

  /**
   * The description for this hotspot.
   *
   * @var string
   */
  protected $description;

  /**
   * The images for this hotspot.
   *
   * @var string
   */
  protected $images = array();

  /**
   * Implements CqHotspotInterface::getIdentifier().
   */
  public function getIdentifier() {
    return $this->identifier;
  }

  /**
   * Implements CqHotspotInterface::setDescription().
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * Implements CqHotspotInterface::getDescription().
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Implements CqHotspotInterface::addImage().
   */
  public function addImage($type, $url, $offset) {
    $offset = $offset == '' ? '0,0' : $offset;
    $this->images[$type] = array(
      'url' => $url,
      'offset' => explode(',', $offset),
    );
  }

  /**
   * Implements CqHotspotInterface::getImages().
   */
  public function getImages() {
    return $this->images;
  }

}
