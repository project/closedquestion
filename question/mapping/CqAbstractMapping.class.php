<?php

/**
 * @file
 * A mapping maps a certain answer into a certain feedback.
 *
 * This is currently used by Check, Drag&Drop, Hotspot and Fillblanks questions.
 */

/**
 * Functions as base for all mapping classes, and as XML DOMElements factory.
 */
abstract class CqAbstractMapping {

  /**
   * The object that the mapping can query.
   *
   * @var CqQuestion
   */
  public $context;

  /**
   * The top-branch of this mapping tree. Usually a CqMapping object.
   *
   * @var CqMapping
   */
  public $topParent;

  /**
   * The parameters of the mapping, in the xml these are the attributes.
   *
   * @var associative array of key/value pairs.
   */
  public $params;

  /**
   * Child-mappings of this mapping.
   *
   * @var array of CqAbstractMapping
   */
  public $children;

  /**
   * Feedback items contained in this mapping.
   *
   * @var array of CqFeedback
   */
  public $feedback;

  /**
   * Evaluate the mapping.
   *
   * Runs any checks in this mapping and returns the result of the checks.
   *
   * @return bool
   *   TRUE if the mapping matches, FALSE if it does not match.
   */
  abstract public function evaluate();

  /**
   * Initialises a mapping item using data from an XML node.
   *
   * @param object $node
   *   DOMElement The node to use for initialisation.
   * @param object $context
   *   CqQuestionInterface The question or other object that the mapping can
   *   query for things like the current answer, draggables, hotspots and the
   *   parsing of html.
   * @param object $topParent
   *   The highest parent in this mapping tree.
   */
  public function generateFromNode($node, &$context, $topParent = NULL) {
    $this->params = array();
    $this->children = array();
    $this->feedback = array();
    $this->context = & $context;
    $this->topParent = & $topParent;
    if (is_null($topParent)) {
      $topParent = &$this;
    }

    foreach ($node->attributes as $attrib) {
      $this->params[strtolower($attrib->nodeName)] = $attrib->nodeValue;
    }

    foreach ($node->childNodes as $child) {
      switch ($child->nodeName) {
        case 'feedback':
          $newchild = CqFeedback::newCqFeedback($child, $context);
          $newchild->topParent = & $topParent;
          $this->feedback[] = $newchild;
          // Continue to the next child.
          continue 2;

        case '#text':
        case '#comment':
        case 'comment':
          // Continue to the next child.
          continue 2;

        case 'pattern':
          // Old style patterns are now handled by CqMatch.
          $newchild = new CqMatch();
          $newchild->context = & $context;
          $newchild->params['pattern'] = $child->nodeValue;
          $this->children[] = $newchild;
          // Continue to the next child.
          continue 2;

        case 'mapping':
          $newchild = new CqMapping();
          break;

        case 'and':
          $newchild = new CqMappingAnd();
          break;

        case 'or':
          $newchild = new CqMappingOr();
          break;

        case 'not':
          $newchild = new CqMappingNot();
          break;

        case 'mathexport':
          $newchild = new CqMappingMathExport();
          break;

        case 'combination':
        case 'match':
          $newchild = new CqMatch();
          break;

        case 'range':
          $newchild = new CqMatchRange();
          break;

        default:
          drupal_set_message(t('Unknown node type: @nodename', array('@nodename' => $child->nodeName)));
          // Continue to the next child.
          continue 2;
      }
      $newchild->generateFromNode($child, $context, $topParent);
      $this->children[] = $newchild;
    }
  }

  /**
   * Returns the HTML of this mapping, and it's children, for text review.
   *
   * @return string
   *   The html representation.
   */
  public function getAllText() {
    $retval = array();

    if (count($this->feedback) > 0) {
      $retval['feedback'] = array(
        '#theme' => 'closedquestion_feedback_list',
        'extended' => TRUE,
      );
      foreach ($this->feedback as $fbitem) {
        $retval['feedback']['items'][] = $fbitem->getAllText();
      }
    }

    $retval['children'] = array(
      '#theme' => 'closedquestion_mapping_list',
      'items' => array(),
      'extended' => TRUE,
    );
    if (isset($this->children)) {
      foreach ($this->children as $child) {
        $retval['children']['items'][] = $child->getAllText();
      }
    }

    $retval['#theme'] = 'closedquestion_mapping_item';
    return $retval;
  }

  /**
   * Get the parameter value of the given parameter.
   *
   * If it is not set, the given default value is returned.
   *
   * @param string $param_name
   *   The name of the parameter to fetch.
   * @param string $default
   *   The default value to return if the parameter is not set.
   *
   * @return string
   *   The value of the parameter.
   */
  public function getParam($param_name, $default = NULL) {
    $retval = isset($this->params[$param_name]) ? $this->params[$param_name] : $default;
    return $retval;
  }

}
