<?php

/**
 * @file
 * The interface CqHotspotInterface.
 * Hotspots are area's in an image that have a certain behaviour. They can be
 * part of the correct answer, or have specific feedback.
 */
interface CqHotspotInterface {

  /**
   * Getter for the identifier of the hotspot.
   *
   * @return string
   *   The identifier of the hotspot.
   */
  public function getIdentifier();

  /**
   * Check if the point given by $location is inside the hotspot
   *
   * @param array $location
   *   An array($x, $y) containing the x and y coordinates of the location to
   *   check.
   *
   * @return bool
   *   TRUE if $location is inside the hotspot, FALSE otherwise.
   */
  public function doMatch($location);

  /**
   * Return the image-map html for this hotspot.
   *
   * @return string
   *   The html to use in an html image-map
   */
  public function getMapHtml();

  /**
   * Return the image-map data for this hotspot.
   *
   * @return Array
   *   The array: array("shape": string, "coords": string)
   */
  public function getMapData();

  /**
   * Returns the description for this hotspot. Can for instance be shown as
   * a mouse-over for the hotspot.
   *
   * @return string
   *   The html description.
   */
  public function getDescription();

  /**
   * Sets the description for this hotspot. Can for instance be shown as
   * a mouse-over for the hotspot.
   *
   * @param string $description
   *   The description for this hotspot.
   */
  public function setDescription($description);

  /**
   * Sets an image for this hotspot.
   *
   * @param string $type
   *   Either 'hover' or 'select'.
   * @param string $url
   *   The url for the image.
   * @param array $offset
   *   The offset as x-y array, e.g. [0,10].
   */
  public function addImage($type, $url, $offset);

  /**
   * Returns the image data for this hotspot
   *
   * @param array
   *   Key-value pairs of images and their data, e.g.
   *     [
   *       "hover"=>[
   *         "url"=>"http://mysite.com/image.jpg",
   *         "offset"=>array(-10,20)
   *       ]
   *     ].
   */
  public function getImages();
}
