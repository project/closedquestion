<?php

/**
 * CqAnswerLogEntity Views Controller class.
 */
class CqAnswerLogEntityViewsController extends EntityDefaultViewsController {

  /**
   * Edit or add extra fields to views_data().
   *
   * Based on old closedquestion_views_data() in file
   * "closedquestion.views.inc".
   */
  public function views_data() {
    $data = parent::views_data();
    $data['cq_user_answer_log']['table']['join'] = array(
      'node' => array(
        'left_field' => 'nid',
        'field' => 'nid',
      ),
      'users' => array(
        'left_field' => 'uid',
        'field' => 'uid',
      ),
    );

    $data['cq_user_answer_log']['log_id']['title'] = t('Log Entry ID');
    $data['cq_user_answer_log']['log_id']['help'] = t('The sequence number of this log entry.');

    $data['cq_user_answer_log']['unixtime']['title'] = t('Log Time');
    $data['cq_user_answer_log']['unixtime']['help'] = t('The time/date for this attempt?');
    $data['cq_user_answer_log']['unixtime']['field']['handler'] = 'views_handler_field_date';
    $data['cq_user_answer_log']['unixtime']['argument']['handler'] = 'views_handler_argument_date';
    $data['cq_user_answer_log']['unixtime']['argument']['filter'] = 'views_handler_filter_date';

    $data['cq_user_answer_log']['once_correct']['title'] = t('Once Correct');
    $data['cq_user_answer_log']['once_correct']['help'] = t('Has the user completed this question?');

    $data['cq_user_answer_log']['tries']['title'] = t('Tries');
    $data['cq_user_answer_log']['tries']['help'] = t('Number of times the user gave an answer.');

    $data['cq_user_answer_log']['data']['field']['handler'] = 'views_handler_field_serialized';

    $data['cq_user_answer_log']['nid'] = array(
      'title' => t('Node Id'),
      'help' => t('A reference to the node of the question.'),
      // Because this is a foreign key to the {node} table. This allows us to
      // have, when the view is configured with this relationship, all the fields
      // for the related node available.
      'relationship' => array(
        'base' => 'node',
        'field' => 'nid',
        'handler' => 'views_handler_relationship',
        'label' => t('CqAnswer - Node relation'),
      ),
    );
    $data['cq_user_answer_log']['uid'] = array(
      'title' => t('User Id'),
      'help' => t('A reference to the User.'),
      // Because this is a foreign key to the {node} table. This allows us to
      // have, when the view is configured with this relationship, all the fields
      // for the related node available.
      'relationship' => array(
        'base' => 'users',
        'field' => 'uid',
        'handler' => 'views_handler_relationship',
        'label' => t('CqAnswer - User relation'),
      ),
    );

    return $data;
  }

}
