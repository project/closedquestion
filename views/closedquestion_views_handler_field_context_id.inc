<?php

/**
 * Replace views_handler_field_field for cq_context_id field.
 *
 * Updates render_item() function to replace the human readable version of the
 * context_id string.
 */
class closedquestion_views_handler_field_context_id extends views_handler_field_field {

  /**
   * {@inheritdoc}
   */
  public function render_item($count, $item) {
    $context_id = $item['raw']['value'];
    $context_hr = check_plain($context_id);
    drupal_alter('cq_human_readable_context', $context_hr, $context_id);
    $item['rendered']['#markup'] = $context_hr;
    return render($item['rendered']);
  }

}

