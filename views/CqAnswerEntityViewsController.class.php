<?php

/**
 * CqAnswerEntity Views Controller class.
 */
class CqAnswerEntityViewsController extends EntityDefaultViewsController {

  /**
   * Edit or add extra fields to views_data().
   *
   * Based on old closedquestion_views_data() data in file
   * "closedquestion.views.inc".
   */
  public function views_data() {
    $data = parent::views_data();
    $data['cq_user_answer']['table']['join'] = array(
      'node' => array(
        'left_field' => 'nid',
        'field' => 'nid',
      ),
      'users' => array(
        'left_field' => 'uid',
        'field' => 'uid',
      ),
    );

    $data['cq_user_answer']['unixtime']['title'] = t('Log Time');
    $data['cq_user_answer']['unixtime']['help'] = t('The time/date for this attempt?');
    $data['cq_user_answer']['unixtime']['field']['handler'] = 'views_handler_field_date';
    $data['cq_user_answer']['unixtime']['argument']['handler'] = 'views_handler_argument_date';
    $data['cq_user_answer']['unixtime']['argument']['filter'] = 'views_handler_filter_date';

    $data['cq_user_answer']['once_correct']['title'] = t('Once Correct');
    $data['cq_user_answer']['once_correct']['help'] = t('Has the user completed this question?');

    $data['cq_user_answer']['tries']['title'] = t('Tries');
    $data['cq_user_answer']['tries']['help'] = t('Number of times the user gave an answer.');

    $data['cq_user_answer']['data']['field']['handler'] = 'views_handler_field_serialized';

    $data['cq_user_answer']['nid'] = array(
      'title' => t('Node Id'),
      'help' => t('A reference to the node of the question.'),
      // Because this is a foreign key to the {node} table. This allows us to
      // have, when the view is configured with this relationship, all the fields
      // for the related node available.
      'relationship' => array(
        'base' => 'node',
        'field' => 'nid',
        'handler' => 'views_handler_relationship',
        'label' => t('CqAnswer - Node relation'),
      ),
    );
    $data['cq_user_answer']['uid'] = array(
      'title' => t('User Id'),
      'help' => t('A reference to the User.'),
      // Because this is a foreign key to the {node} table. This allows us to
      // have, when the view is configured with this relationship, all the fields
      // for the related node available.
      'relationship' => array(
        'base' => 'users',
        'field' => 'uid',
        'handler' => 'views_handler_relationship',
        'label' => t('CqAnswer - User relation'),
      ),
    );
    return $data;
  }

}
