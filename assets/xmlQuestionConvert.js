/**
 * @file
 * ClosedQuestion specific functions for the xmlEditor.
 */

/**
 * Make sure we remember when user edits the question using the textarea.
 *
 * @param {object} bodyField
 *  De bodyField textarea DOM object
 */
function CQ_InitBody(bodyField) {
  var events = 'keyup.cq paste.cq';
  jQuery(bodyField).unbind(events);
  /* we should remember when user edits the question using the textarea */
  jQuery(bodyField).bind(events, function () {
    jQuery(this).data('cq_isdirty', true);
  });
}

/**
 * Select the tab that should be initially selected.
 * This is called by Drupal.behaviours.
 */
function CQ_InitialView(context) {
  var bodyField = jQuery('#edit-body-' + Drupal.settings.closedquestion.language + '-0-value', context);
  if (bodyField.length === 0) {
    bodyField = jQuery('#edit-body-und-0-value', context);
  }
  CQ_InitBody(bodyField);

  if (bodyField.length > 0) {
    if (bodyField[0].value.length > 0) {
      CQ_ShowTree();
    }
    else {
      CQ_ShowTemplates()
    }
  }

  /* make sure the field has the correct text format */
  /* @TODO: implement better solution, http://drupal.stackexchange.com/questions/16036/how-do-you-set-a-default-input-text-format-per-content-type */
  var $select = jQuery('#body_field_wrapper .filter-list.form-select');

  if ($select.children('[value=closedquestion]').length === 1) {
    $select.val('closedquestion').attr('data-val-set-by-xmleditor', 'true');
    $select.trigger('change');
    $select.closest('.fieldset-wrapper').hide();
  }


  /* listen to editor updates */
  jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('bind', 'change', function () {
    jQuery(bodyField).data('cq_isdirty', false);
  });
}

/**
 * Find the body field.
 *
 * @param lang
 *   The suggested language to use to search for the body field. If no body
 *   field with this language is found, "und" is used. If that also results in
 *   no match, the first textarea with a name that starts with 'body' is used.
 */
function CQ_FindBodyField(lang) {
  var bodyFieldSelector = '#edit-body-' + lang + '-0-value';
  var bodyField = jQuery(bodyFieldSelector)[0];
  if (bodyField == undefined) {
    bodyFieldSelector = '#edit-body-und-0-value';
    bodyField = jQuery(bodyFieldSelector)[0];
  }
  if (bodyField == undefined) {
    bodyField = jQuery('textarea[name^="body"]')[0];
  }
  return bodyField;
}

/**
 * Called before form submit
 * @returns
 */
function CQ_beforesubmit() {
  var bodyField = CQ_FindBodyField(Drupal.settings.closedquestion.language);
  if (jQuery(bodyField).data('cq_isdirty') === false || jQuery(bodyField).data('cq_isdirty') === undefined) {
    /* user has not modified the xml using the bodyField: get latest version of xml from tree editor */
    bodyField.value = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('read');
  }
  return false;
}

function CQ_AddHelpAndCSS(questionType) {
  /* add attribute to container to later style it with CSS */
  var container = jQuery('#xmlJsonEditor_container');
  container.attr('data-question-type', questionType);

}

/**
 * Shows the editor tab, loads the current XML into the editor and hides the
 * XML and templates tabs.
 */
function CQ_ShowTree(forceInit) {
  var lang = Drupal.settings.closedquestion.language;
  var bodyField = CQ_FindBodyField(lang);
  var initEditor = (forceInit || (
    bodyField.value.trim().length > 0
    && (jQuery('#xmlJsonEditor_tree_container').attr('data-xmleditor-is-init') !== 'true'
      || jQuery(bodyField).data('cq_isdirty') === true)
  ));

  jQuery('#xmlJsonEditor_tree').addClass('xmlJsonEditor_activeTab');
  jQuery('#xmlJsonEditor_tree').removeClass('xmlJsonEditor_inactiveTab');
  jQuery('#xmlJsonEditor_xml').addClass('xmlJsonEditor_inactiveTab');
  jQuery('#xmlJsonEditor_xml').removeClass('xmlJsonEditor_activeTab');
  jQuery('#xmlJsonEditor_templates').addClass('xmlJsonEditor_inactiveTab');
  jQuery('#xmlJsonEditor_templates').removeClass('xmlJsonEditor_activeTab');

  jQuery('#edit-body').css('display', 'none');
  jQuery('#xmlJsonEditor_container').css('display', 'block');
  jQuery('#xmlJsonEditor_template_container').css('display', 'none');
  jQuery('#xmlJsonEditor_tree_container').closest('form');

  if (!initEditor) {
    return;
  }
  jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('init', '#xmlJsonEditor_editor', bodyField.value, Drupal.settings.closedquestion.xmlConfig);

  /**
   * Add links to tutorials etc based on question type
   */
  jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('bind', 'change', function (data) {
    if (data.type === "question" && data.what === "attribute" && data.which === "type") {
      CQ_AddHelpAndCSS(data.value);
    }
  });
  var questionTags = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('search', 'question');
  if (questionTags && questionTags[0] && questionTags[0].data) {
    if (questionTags[0].data().jstree_cq && questionTags[0].data().jstree_cq.attributes) {
      CQ_AddHelpAndCSS(questionTags[0].data().jstree_cq.attributes.type);
    }
  }

  /**
   * Convert inlinechoice tags to mathresult tags for feedback items in
   * fillblanks questions.
   */
  jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('bind', 'onLoadEditor', function (data) {
    var parent = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('closest', data.treeNode, 'question');
    var questionType = parent.data('jstree_cq').attributes.type;

    if (questionType === 'fillblanks' && data.type === 'feedback') {
      /* Detect changes in textarea and replace <inlinechoice> tags with
       * <mathresult> tags.
       */
      jQuery('#cq_editor_content').bind('change', function (e) {
        var $this = jQuery(this);
        var currentValue = $this.val();
        var newValue = currentValue
          .replace(/<inlinechoice([^>])* id="/ig, '<mathresult e="')
          .replace(/(<mathresult([^>])*)freeform="\d"/ig, '$1')
          .replace(/(<mathresult([^>])*)longtext="\d"/ig, '$1');
        if (newValue !== currentValue) {
          $this.val(newValue);
          $this.trigger('keyup');
        }
      });
    }
  });

  /**
   * plugin matchImg editor
   */


  /**
   * Returns the match image URL
   *
   * @param {object} treeNode
   *
   * @returns {String|Boolean}
   */
  function getMatchImgData(treeNode) {
    var imageData = {};

    var parent = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('closest', treeNode, 'question');
    var matchImage = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('search', 'matchimg', {"parent": parent});
    var imageSrc, re, matchImageData;

    if (matchImage[0] !== undefined) {
      matchImageData = matchImage[0].data();
      imageSrc = matchImageData.jstree_cq.attributes.src;
    }

    if (imageSrc && imageSrc.indexOf('[attachurl') === 0) {

      // Try to get url from server.
      var imageUrl = CQ_ParseText(imageSrc, '#xmlJsonEditor_container');
      if (imageUrl === 'filenotfound') {
        // No luck, try to get file path from File attachment DOM container
        // element's fid attribute.
        re = new RegExp('\attachurl:([^\[]*)\]', 'ig');
        var imageSrcResult = re.exec(matchImageData.jstree_cq.attributes.src)[1];
        var $fa_imagecontainer = jQuery(".media-item[title='" + imageSrcResult + "']");
        if ($fa_imagecontainer.length > 0) {
          var fid = $fa_imagecontainer.attr('data-fid');
          imageData.url = Drupal.settings.basePath + 'closedquestion/getmedia/' + fid;
        }
      }
    }
    else if (imageSrc && imageSrc.indexOf('[[{') === 0) {
      // Assume Media tag: get fid.
      re = new RegExp('"fid":([^,]*),');
      var fid = re.exec(matchImageData.jstree_cq.attributes.src)[1];
      imageData.url = Drupal.settings.basePath + 'closedquestion/getmedia/' + fid;
    }

    if (imageData.url) {
      imageData.width = matchImageData.jstree_cq.attributes.width;
      imageData.height = matchImageData.jstree_cq.attributes.height;
    }

    return Object.keys(imageData).length > 0 ? imageData : false;
  }

  /**
   * Handle 'power matches': regular expressions added by the editor to
   * automatically generated
   * <select> boxes. For fillblanks type of questions we want to hide another
   * form item if no power match has been selected (matchall).
   */
  jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('bind', 'onLoadEditor', function (data) {
    // Only continue for fillblanks questions and while editing 'range' nodes.
    var parent = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('closest', data.treeNode, 'question');
    if (!data.type === 'fillblanks' || parent.data('jstree_cq').attributes.type === 'range') {
      return;
    }

    // Only continue when we find a inlinechoice attribute editor element.
    var $answerContainerSelect = jQuery('#xmlEditor_inlinechoice');
    if ($answerContainerSelect.length === 0) {
      return;
    }

    // Handler function, shows/hides matchall form item.
    function handlePowerMatch() {
      var $answerContainerSelect = jQuery('#xmlEditor_inlinechoice');
      if ($answerContainerSelect && $answerContainerSelect.find('[data-id="power_matches"]')) {
        var $selectedOption = $answerContainerSelect.find('option:selected');
        if ($selectedOption && $selectedOption.closest('optgroup[data-id="power_matches"]').length > 0) {
          jQuery('.xmlJsonEditor_attribute_container[data-attribute="matchall"]').show();
        }
        else {
          jQuery('.xmlJsonEditor_attribute_container[data-attribute="matchall"]').hide();
        }
      }
    }

    $answerContainerSelect.on('change', handlePowerMatch);
    handlePowerMatch();
  });

  /* bind to load attribute editor */
  jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('bind', 'onLoadEditor', function (data) {
    var miEditorConfig = {};
    var matchImageData = {};

    jQuery('.matchImgEditor').remove();
    jQuery('.xmlJsonEditor_form_description .miFeedback').remove();

    var parent = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('closest', data.treeNode, 'question');
    var questionType = parent.data('jstree_cq').attributes.type;

    /* Filter out question types and tag types */
    if (['arrow', 'dragdrop', 'hotspot'].indexOf(questionType) === -1) {
      return;
    }
    if (['hotspot', 'match', 'startstate'].indexOf(data.type) === -1) {
      return true;
    }

    // Get match image data.
    matchImageData = getMatchImgData(data.treeNode);
    if (matchImageData) {
      miEditorConfig.imageURL = matchImageData.url;
    }
    else {
      jQuery('.xmlJsonEditor_form_description').append('<div class="xmlEditorAttributeFeedback_error miFeedback" style="padding:0.3em">' + Drupal.t('No image found. Please add an image to the question first.') + '</div>');
      return;
    }

    if (data.type === "hotspot") {
      /* Create hotspot editor. */

      // Config editor.
      var shapeSelect = jQuery(data.editorElements.forElement.attributeEditorElements).find("#xmlEditor_shape")[0];
      var $coordsFormElement = jQuery(data.editorElements.forElement.attributeEditorElements).find("#xmlEditor_coords");
      var $coordsFeedback = jQuery('<li class="miEditorFeedback xmlJsonEditor_feedback_description"></li>');
      $coordsFormElement.parent().find('.xmlEditorAttributeFeedback ul').append($coordsFeedback);

      $coordsFormElement.hide();
      $coordsFormElement.parent().find('.xmlEditorAttributeFeedback').hide();

      var shape = jQuery(shapeSelect).val();
      var coords = $coordsFormElement.val();

      miEditorConfig.imageFormElements = data.editorElements.forElement.attributeEditorElements;
      miEditorConfig.shapeSelect = shapeSelect;
      miEditorConfig.mode = 'edit hotspot';
      miEditorConfig.insertBeforeElement = jQuery('#xmlEditor_coords');
      miEditorConfig.imageWidth = matchImageData.width;
      miEditorConfig.imageHeight = matchImageData.height;
      /* create matchImgEditor */
      var miEditor = new matchImgEditor(jQuery('#editor_values'), miEditorConfig);
      if (miEditor.config.imageURL === undefined) {
        return true;
      }

      /* bind to ondrawshape event to detect changes */
      miEditor.bind('onDrawShape', function (hotspotData) {
        var dataAsString = '';
        var dataAsArray = [];
        var imageDimensions;
        var lx, ly, hx, hy;
        switch (hotspotData.shape) {
          case 'circle':
            dataAsString = parseInt(hotspotData.coords.x) + ',' + parseInt(hotspotData.coords.y) + ',' + hotspotData.radius;
            imageDimensions = hotspotData.radius * 2 + 'x' + hotspotData.radius * 2;
            break;

          default:
            jQuery(hotspotData.coords).each(function () {
              dataAsArray.push(parseInt(this.x) + ',' + parseInt(this.y));
              lx = (lx === undefined || this.x < lx) ? this.x : lx;
              ly = (ly === undefined || this.y < ly) ? this.y : ly;
              hx = (hx === undefined || this.x > hx) ? this.x : hx;
              hy = (hy === undefined || this.y > hy) ? this.y : hy;
            });
            dataAsString = dataAsArray.join(',');
            imageDimensions = (hx - lx) + 'x' + (hy - ly);
            break;
        }

        $coordsFormElement.val(dataAsString).trigger('change');
        $coordsFeedback.html('Hotspot dimensions: ' + imageDimensions + ' pixels');
      });

      //set the selected shape
      jQuery(shapeSelect).change(function () {
        miEditor.clearCanvas();
        miEditor.setCurrentShape(jQuery(this).val());
      });
      miEditor.clearCanvas();
      miEditor.setCurrentShape(shape);

      //draw the shape
      if (coords !== '') {
        miEditor.loadShape({
          "shape": shape,
          "coords": coords
        });
      }
      return false;
    }
    else if (data.type === "match" || data.type === "startstate") {
      miEditorConfig.imageFormElements = data.editorElements.forElement.attributeEditorElements;
      miEditorConfig.shapeSelect = shapeSelect;
      miEditorConfig.mode = 'view hotspots';
      if (data.type === 'match') {
        miEditorConfig.insertBeforeElement = jQuery('#xmlEditor_hotspot');
        miEditorConfig.className = 'matchAttributeEditor';
      }
      else {
        miEditorConfig.insertBeforeElement = jQuery('#xmlEditor_value').hide();
      }
      miEditorConfig.imageWidth = matchImageData.width;
      miEditorConfig.imageHeight = matchImageData.height;

      /* create matchImgEditor */
      miEditor = new matchImgEditor(jQuery('#editor_values'), miEditorConfig);
      if (miEditor.config.imageURL === undefined) {
        return true;
      }

      /* get hotspots, and draw them on the image */
      var hotspots = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('search', 'hotspot');
      var hotspotTitleIds = [];
      // Time is used to create unique id's
      var time = new Date().getTime();
      var $formElement = jQuery('#xmlEditor_hotspot');

      jQuery(hotspots).each(function () {
        /* get data */
        var hotspotsData = this.data().jstree_cq.attributes;

        //keep track of titles, so we can update them when matched
        var hotspotTitleId = 'xmlEditor_' + time + '_' + hotspotsData.id;
        hotspotTitleIds.push(hotspotTitleId);

        /* add to editor */
        miEditor.loadShape({
          "shape": hotspotsData.shape,
          "coords": hotspotsData.coords,
          "title": '<span id="' + hotspotTitleId + '" data-value="' + hotspotsData.id + '">' + hotspotsData.id + '</span>'
        }, true);

        // Let hotspot title click change form element.
        jQuery('#' + hotspotTitleId).bind('click', function () {
          $formElement.val(jQuery(this).text());
          $formElement.trigger('change');
        });
      });


      var highlightSelectedShapeHandler = function () {
        var $formElement = jQuery('#xmlEditor_hotspot');
        var value = $formElement.val();
        var valueRegExp = new RegExp(value);
        jQuery('.xmlJsonEditor_hotspot_tag', $formElement.closest('form')).removeClass('xmlJsonEditor_matched');

        // Use regex to add class to matching tags.
        jQuery('[data-value]').each(function () {
          var $this = jQuery(this);
          var attValue = $this.attr('data-value');
          if (valueRegExp.test(attValue)) {
            $this.parent().addClass('xmlJsonEditor_matched');
          }
        });
      };
      $formElement.bind('change', highlightSelectedShapeHandler);
      highlightSelectedShapeHandler();

      /* Start state editor */
      if (data.type === "startstate") {
        var question = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('search', 'question')[0];
        var questionAtts = question.data().jstree_cq.attributes;
        if (question.data().jstree_cq.attributes) {
          jQuery('#editor_values').removeAttr('class').attr('class', questionAtts.cssclass); // Transparent draggables class
        }

        var treeDraggables = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('search', 'draggable');
        var $container = jQuery('<div id="cq_editoranswerContainer" />');
        var $sourceElement = jQuery("#xmlEditor_value");
        var currentValue = $sourceElement.val();
        var ddDraggableStartPos = [];
        var currentY = 0;

        jQuery('.matchImgEditorImage').wrap($container);

        jQuery(treeDraggables).each(function () {
          var matches, inlineTag, imgHTML, i;
          /* get data */
          var data = this.data().jstree_cq;
          var id = data.attributes.id;
          var html = data.content;

          // Replace Media tags with HTML tags.
          if (CQ_MediaModule2IsEnabled() === true) {
            matches = html.match(/\[\[.*?\]\]/g);
            if (matches) {
              inlineTag = "";
              for (i = 0; i < matches.length; i++) {
                inlineTag = matches[i];
                imgHTML = CQ_ImgHtmlFromMediatag(inlineTag);
                html = html.replace(inlineTag, imgHTML);
              }
            }
          }

          // Replace self-closing tags with full tags
          html = html.replace(/<\s*([^\s>]+)([^>]*)\/\s*>/g, '<$1$2></$1>');

          var $draggable = jQuery('<div cqvalue="' + id + '" class="cqDdDraggable">' + html + '</div>');
          jQuery('#cq_editoranswerContainer').append($draggable);
        });

        /* Handle the case in which there is no value set. */
        if (!currentValue || currentValue.length === 0) {
          currentValue = '';
        }

        // Check if all draggables defined in the value are still present.
        var draggablePosArray = currentValue.split(';');
        var draggablePosObject = {};
        jQuery.each(draggablePosArray, function (i, posAsString) {
          var posAsArray = posAsString.split(',');
          if (posAsArray.length === 3 && jQuery('#cq_editoranswerContainer .cqDdDraggable[cqvalue="' + posAsArray[0] + '"]').length > 0) {
            draggablePosObject[posAsArray[0]] = {
              "cqvalue": posAsArray[0],
              "x": parseInt(posAsArray[1], 10),
              "y": parseInt(posAsArray[2], 10)
            };
          }
        });

        // Position draggables, taking into account new draggables.
        jQuery(treeDraggables).each(function (i, treeDraggable) {
          var data = treeDraggable.data().jstree_cq;
          var id = data.attributes.id;
          var $draggableGUI = jQuery('#cq_editoranswerContainer .cqDdDraggable[cqvalue="' + id + '"]');

          if (!draggablePosObject[id]) {
            var draggableGUIWidth = $draggableGUI.width();
            var draggableGUIHeight = $draggableGUI.height();

            currentY = currentY === 0 ? draggableGUIHeight / 2 : currentY;

            // Add settings to answer string.
            currentValue += id + ',' + (draggableGUIWidth / 2) + ',' + (currentY) + ';';

            draggablePosObject[id] = {
              "cqvalue": id,
              "x": parseInt(draggableGUIWidth / 2, 10),
              "y": parseInt(currentY, 10)
            };

            currentY += draggableGUIHeight + 2;
          }
        });

        //Get new value for for element.
        currentValue = '';
        jQuery.each(draggablePosObject, function (id, posAsObject) {
          ddDraggableStartPos.push(posAsObject);
          currentValue += posAsObject.cqvalue + ',' + posAsObject.x + ',' + posAsObject.y + ';';
        });

        // Init question.
        cqInitDDQuestion({
          "ddImage": {
            "url": miEditorConfig.imageURL,
            "width": miEditorConfig.imageWidth,
            "height": miEditorConfig.imageHeight
          },
          "questionid": 'cq_editor',
          "ddDraggableStartPos": ddDraggableStartPos,
          "formElement": $sourceElement
        });

        // Finaly, update form element.
        $sourceElement.val(currentValue);
        window.setTimeout(function () {
          $sourceElement.trigger('change'); // For some reason this doesn't
                                            // work directly @todo fix.
        }, 300);
      }

      return false;
    }
  });

  /* Init Arrow editor */
  jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('bind', 'onLoadEditor', function (data) {
    var question = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('closest', data.treeNode, 'question');
    var questionConfig = question.data('jstree_cq');
    var questionType = questionConfig.attributes.type;
    var cssclass = questionConfig.attributes.cssclass ? questionConfig.attributes.cssclass : '';
    var $wrapper;
    var matchImageData, arrowEditorConfig = {};
    var $matchTypeSelectorWrapped = jQuery('<div class="arrowEditorMatchTypeSelector"><select><option value="exact">' + Drupal.t('This is the complete answer.') + '</option><option value="part">' + Drupal.t('This is part of the answer.') + '</option></select></div>');
    var $matchTypeSelector = $matchTypeSelectorWrapped.find('select');
    var isEditing = false;

    /* Only arrow questions allowed from here. */
    if (['arrow'].indexOf(questionType) === -1) {
      return;
    }

    if (data.type === "match") {
      jQuery('#xmlEditor_presets_select').hide();
      jQuery('.arrowEditor').remove();

      var $sourceElement = jQuery("#xmlEditor_pattern");
      var currentAnswer = $sourceElement.val();

      var $editorContainer = jQuery('<div class="arrowEditor"></div>');
      var $sourceElementDescription = jQuery('<div class="arrowEditorFeedback xmlJsonEditor_feedback_description"></div>');

      var $parent = $sourceElement.closest('.xmlJsonEditor_attribute_container_wrap');
      $wrapper = $parent.append('<div class="arrowEditorWrapper" />').find('.arrowEditorWrapper');

      $wrapper.prepend($editorContainer);

      // Determine whether an answer can be interpreted by the Arrow editor.
      var checkAnswerWysiwyg = function (answer) {
        var re = new RegExp('^\\^?([a-zA-Z]{2},?)*\\$?$', 'ig');
        $sourceElementDescription.find('.arrowEditor_error').remove();
        if (!re.test(answer)) {
          $sourceElementDescription.append('<p style="color:red;" class="arrowEditor_error">' + Drupal.t('This advanced condition is not (yet) supported by the editor. The editor might not display the condition correctly. Please use below input.') + '</p>');

          if ($parent.find('.arrowEditor_toggle').hasClass('arrowEditor_simple')) {
            $parent.find('.arrowEditor_toggle').trigger('click');
          }
          return false;
        }
      };

      // Get question data.
      var questionNode = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('closest', data.treeNode, 'question');
      var questionNodeAtts = questionNode.data().jstree_cq.attributes;

      // Get hotspots data.
      var hotspotNodes = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('search', 'hotspot', {"parent": questionNode});
      var hotspotConfig = {};
      jQuery.each(hotspotNodes, function (i, hotspotNode) {
        var hotspotAtts = hotspotNode.data().jstree_cq.attributes;
        hotspotConfig[hotspotAtts.id] = {
          "shape": hotspotAtts.shape,
          "coords": hotspotAtts.coords
        };
      });

      if (hotspotNodes && hotspotNodes.length > 0) {
        let $optgroup = jQuery('<optgroup label="Power matches"></optgroup>');

        $matchTypeSelector.append($optgroup);

        jQuery.each(hotspotNodes, function (i, hotspotNode) {
          var hotspotAtts = hotspotNode.data().jstree_cq.attributes;
          let $option = jQuery('<option />');
          $option.attr('name', 'power_matches');
          $option.html(Drupal.t("Any arrow to or from hotspot") + " '" + hotspotAtts.id + "'");
          $option.attr('value', hotspotAtts.id)
          $optgroup.append($option);
        });
      }

      // Get image data and add image to DOM.
      matchImageData = getMatchImgData(data.treeNode);
      if (matchImageData.url) {
        arrowEditorConfig.imageURL = matchImageData.url;
      }
      else {
        jQuery('.xmlJsonEditor_form_description').append('<div class="xmlEditorAttributeFeedback_error miFeedback" style="padding:0.3em">' + Drupal.t('No image found. Please add an image to the question first.') + '</div>');
        return;
      }

      var $image_element = jQuery('<img />');
      $image_element.attr('src', matchImageData.url);
      $editorContainer.append($image_element, $matchTypeSelectorWrapped);
      $sourceElementDescription.insertBefore($matchTypeSelectorWrapped);
      /* Bring editor to life */

      // Initialize the question.
      var questionConfig = {
        "ddImage": {
          "height": matchImageData.height,
          "width": matchImageData.width,
          "url": matchImageData.url
        },
        "startArrow": (questionNodeAtts.startarrow === "yes"),
        "endArrow": (questionNodeAtts.endarrow === "yes"),
        "hotspots": hotspotConfig,
        "lineNumbering": (questionNodeAtts.linenumbering === "yes"),
        "lineStyle": questionNodeAtts.linestyle,
        "showHotspotLabels": true
      };

      var cqArrowQuestionPromise = cqArrowQuestion($image_element, questionConfig);

      var onUpdateAnswerFromCqArrowQuestion = function () {
        if (isEditing) {
          return;
        }
        let cqArrowQuestion = $image_element.data('cqArrowQuestion');
        let matchType = $matchTypeSelector.val();
        let answer = cqArrowQuestion.getAnswer();

        // Set match type to 'exact' if answer is no power match but match type
        // is set on power match.
        if (answer !== '' && answer.length > 1 && ['exact', 'part'].indexOf(matchType) === -1) {
          matchType = 'exact'
        }

        // Set match type to 'exact' if answer is empty.
        if (answer === '') {
          matchType = 'exact';
        }

        // Set match type to match power match.
        if (answer.length === 1) {
          matchType = answer;
        }

        // Add regex stuff to match exact answer.
        if (matchType === 'exact') {
          answer = '^' + answer + '$';
        }
        $matchTypeSelector.val(matchType);
        $sourceElement.val(answer);
        $sourceElement.trigger('change');
      };

      cqArrowQuestionPromise.then(function () {
        /* set current answer when plugin is inited */
        var cqArrowQuestion = $image_element.data('cqArrowQuestion');
        var answerSanitized = currentAnswer.replace(/[\^$]/ig, '');

        cqArrowQuestion.setAnswer(answerSanitized);

        /* create handler to connect plugin to closed question form */

        /* let question tell us when user updates answer */
        cqArrowQuestion.registerPlugin("submitFeedback", {
          "hooks": {
            "onAddLineToAnswer": onUpdateAnswerFromCqArrowQuestion,
            "onRemoveLineFromAnswer": onUpdateAnswerFromCqArrowQuestion
          }
        });
      });


      // Bring the match type selector to life.
      $matchTypeSelector.on('change', function (e, silent) {
        let $this = jQuery(this);
        let matchType = $this.val();

        if (questionConfig.lineNumbering && matchType === 'part') {
          $sourceElementDescription.html(Drupal.t('Note: As this is the partial answer, above arrow numbering is relative. Arrows numbered e.g. "1" and "2" will also match with arrows numbered "3" and "4", or "7" and "8", etc.'));
        }
        else {
          $sourceElementDescription.html('');
        }

        if (silent !== true) {
          let cqArrowQuestion = $image_element.data('cqArrowQuestion');
          let answer = cqArrowQuestion.getAnswer();

          if (matchType === 'exact') {
            answer = '^' + answer + '$';
          }
          else if (matchType !== 'part') {
            // Power match.
            answer = matchType;
            cqArrowQuestion.setAnswer(answer)
          }
          $sourceElement.val(answer);
          $sourceElement.trigger('change');
        }
        ;
      });

      var $sourceElementWrapper = jQuery('<div class="soEditor_source_element_wrapper" style="display:none;border: 1px dashed rgb(238, 238, 238); padding: 0.5em;" />');
      var $sourceElementToggle = jQuery('<a href="javascript:void(0)" class="arrowEditor_toggle arrowEditor_simple"><span style="display: none;">Hide advanced</span><span style="">Advanced</span></a>');
      $sourceElementWrapper.append('<p class="xmlEditorAttributeFeedback" style="margin-bottom: 0;">The condition as it is stored in the database:</p>', $sourceElement, $wrapper.find('.xmlEditorAttributeFeedback'));
      $wrapper.prepend($editorContainer, $sourceElementToggle, $sourceElementWrapper);

      $sourceElementToggle.bind('click', function () {
        var $this = jQuery(this);
        $this.toggleClass('arrowEditor_simple');
        $this.find('span').toggle();
        $sourceElementWrapper.toggle();
      });

      // Add check for user input
      checkAnswerWysiwyg(currentAnswer);
      $sourceElement.on('keyup', function () {
        var currentAnswer = jQuery(this).val().replace(/[]/ig, '');
        var answerSanitized = currentAnswer.replace(/[\^$]/ig, '');
        if (checkAnswerWysiwyg(answerSanitized)) {
          isEditing = true;
          $image_element.data('cqArrowQuestion').setAnswer(answerSanitized);
          isEditing = false;
        }
      });

      // Set type selector value based on current input.
      if (currentAnswer.indexOf('^') === 0 || currentAnswer.indexOf('$') >= 0) {
        $matchTypeSelector.val('exact');
      }
      else if (currentAnswer.length > 1) {
        $matchTypeSelector.val('part');
      }
      else {
        // Power match.
        $matchTypeSelector.val(currentAnswer);
      }
      $matchTypeSelector.trigger('change', [true]);
    }
  });

  /* Init SO editor */
  jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('bind', 'onLoadEditor', function (data) {
    var question = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('closest', data.treeNode, 'question');
    var questionConfig = question.data('jstree_cq');
    var questionType = questionConfig.attributes.type;
    var question_options = [];
    var question_targets = [];

    /* Only select&order questions allowed from here. */
    if (['selectorder'].indexOf(questionType) === -1) {
      return;
    }

    if (data.type === "match" || data.type === "startstate") {
      jQuery('#soEditor').remove();
      var $editorContainer = jQuery('<div id="soEditor"></div>');
      var $sourceElement;
      var $sourceElementWrapper = jQuery('<div class="soEditor_source_element_wrapper" style="display:none;border: 1px dashed rgb(238, 238, 238); padding: 0.5em;" />');
      var $sourceElementContainer, $sourceElementToggle;
      var onlyOrder;
      var targets = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('search', 'target');

      var options = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('search', 'option');
      jQuery.each(options, function () {
        let option_data = jQuery(this).data('jstree_cq');
        let text_el = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('search', 'choice', {"parent": this})[0];
        let text_data = jQuery(text_el).data('jstree_cq');
        let allowedTags = jQuery('#xmlJsonEditor_tree_container').data('allowedTags');

        if (jQuery.isNumeric(option_data.attributes.id)) {
          /* Targets, for backwards compatibility only, replaced by <target> */
          question_targets.push({
            "id": option_data.attributes.id,
            "label": text_data.content
          });
        }
        else {
          // Collapse some custom tags to prevent jQuery 3.5.0 stripping
          // contents.
          let label = text_data.content;
          jQuery.each(allowedTags, function (i, tag) {
            label = label.replace(new RegExp('(<\s*(' + tag + ')([^>]*?))\/>', 'gi'), '$1></$2>');
          });

          // Add option to list.
          question_options.push({
            "id": option_data.attributes.id,
            "label": label
          });
        }
      });

      /* Targets. */
      jQuery.each(targets, function () {
        var option_data = jQuery(this).data('jstree_cq');
        var text_el = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('search', 'choice', {"parent": this})[0];
        var text_data = jQuery(text_el).data('jstree_cq');
        question_targets.push({
          "id": option_data.attributes.id,
          "label": text_data.content
        });

      });

      if (data.type === "match") {
        $sourceElement = jQuery("#xmlEditor_pattern");
      }
      else {
        $sourceElement = jQuery("#xmlEditor_value");
      }
      $sourceElementContainer = $sourceElement.closest('.xmlJsonEditor_attribute_container_wrap');

      $sourceElementToggle = jQuery('<a href="javascript:void(0)"><span style="display: none;">Hide advanced</span><span style="">Advanced</span></a>');

      $sourceElementContainer.prepend($editorContainer, $sourceElementToggle, $sourceElementWrapper);

      $sourceElementToggle.bind('click', function () {
        jQuery(this).find('span').toggle();
        $sourceElementWrapper.toggle();
      });

      // Wrap input in div, so we can show/hide it.

      $sourceElementWrapper.append('<p class="xmlEditorAttributeFeedback" style="margin-bottom: 0;">The condition as it is stored in the database:</p>', $sourceElement, $sourceElementContainer.find('.xmlEditorAttributeFeedback'));


//      $sourceElement.before();

      // Init editor
      if (parseInt(questionConfig.attributes.onlyorder, 10) === 2 && data.type === "startstate") {
        // Start state editor should not create special match for order does
        // not matter questions.
        onlyOrder = -1;
      }
      else {
        onlyOrder = questionConfig.attributes.onlyorder;
      }
      $editorContainer.soEditor({
        "input": $sourceElement,
        "question_options": question_options,
        "question_targets": question_targets,
        "allowDuplicates": questionConfig.attributes.duplicates === "1",
        "horizontalAlignment": questionConfig.attributes.alignment === "horizontal",
        "onlyOrder": onlyOrder,
        "allowSpecialElements": (data.type === "match") ? true : false
      });

      // Update attribute title
      $sourceElement.closest('tr.xmlJsonEditor_attribute').find('.xmlJsonEditor_attribute_title').html('Condition');
    }
  });

  jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('bind', 'onLoadEditor', function (data) {
    var question = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('closest', data.treeNode, 'question');
    var questionConfig = question.data('jstree_cq');
    var questionType = questionConfig.attributes.type;
    var question_options = [];
    var cssclass = questionConfig.attributes.cssclass ? questionConfig.attributes.cssclass : '';

    var $wrapper;
    /* Only multiple answer questions allowed from here. */
    if (['check'].indexOf(questionType.toLowerCase()) === -1) {
      return;
    }

    if (data.type === "match") {
      jQuery('#maEditor').remove();
      var $editorContainer = jQuery('<div id="maEditor" class="' + cssclass + '"></div>');
      var sourceElement;
      var options = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('search', 'option');
      jQuery.each(options, function () {
        var option_data = jQuery(this).data('jstree_cq');
        var text_el = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('search', 'choice', {"parent": this})[0];
        var text_data = jQuery(text_el).data('jstree_cq');

        question_options.push({
          "id": option_data.attributes.id,
          "label": text_data.content
        });
      });


      sourceElement = jQuery("#xmlEditor_pattern");

      $wrapper = sourceElement.closest('.xmlJsonEditor_attribute_container_wrap');

      $wrapper.prepend($editorContainer);
      $editorContainer.maEditor({
        "input": sourceElement,
        "question_options": question_options
      });
    }
  });

  /* Click first item (@todo: fix the need for this ugly timeout) */
  window.setTimeout(function () {
    jQuery('#xmlEditor_1 a').first().click();
  }, 100);

  return true;
}

/**
 * Shows the XML tab and hides the editor and templates tabs.
 */
function CQ_ShowXML() {
  var bodyField = CQ_FindBodyField(Drupal.settings.closedquestion.language);
  try {
    bodyField.value = jQuery('#xmlJsonEditor_tree_container').xmlTreeEditor('read');
    jQuery(bodyField).data('cq_isdirty', false);
  }
  catch (e) {
    console.error(e);
  }

  jQuery('#xmlJsonEditor_tree').addClass('xmlJsonEditor_inactiveTab');
  jQuery('#xmlJsonEditor_tree').removeClass('xmlJsonEditor_activeTab');
  jQuery('#xmlJsonEditor_xml').addClass('xmlJsonEditor_activeTab');
  jQuery('#xmlJsonEditor_xml').removeClass('xmlJsonEditor_inactiveTab');
  jQuery('#xmlJsonEditor_templates').addClass('xmlJsonEditor_inactiveTab');
  jQuery('#xmlJsonEditor_templates').removeClass('xmlJsonEditor_activeTab');

  jQuery('#xmlJsonEditor_container').css('display', 'none');
  jQuery('#edit-body').css('display', 'block');
  jQuery('#xmlJsonEditor_template_container').css('display', 'none');
}

/**
 * Shows the templates tab and hides the editor and XML tabs.
 */
function CQ_ShowTemplates() {
  jQuery('#xmlJsonEditor_tree').addClass('xmlJsonEditor_inactiveTab');
  jQuery('#xmlJsonEditor_tree').removeClass('xmlJsonEditor_activeTab');
  jQuery('#xmlJsonEditor_xml').addClass('xmlJsonEditor_inactiveTab');
  jQuery('#xmlJsonEditor_xml').removeClass('xmlJsonEditor_activeTab');
  jQuery('#xmlJsonEditor_templates').addClass('xmlJsonEditor_activeTab');
  jQuery('#xmlJsonEditor_templates').removeClass('xmlJsonEditor_inactiveTab');

  jQuery('#xmlJsonEditor_container').css('display', 'none');
  jQuery('#edit-body').css('display', 'none');
  jQuery('#xmlJsonEditor_template_container').css('display', 'block');
}

/**
 * Posts the text, with the form-id to the server so the server can parse the
 * text for any tags, and return the replaced text.
 *
 * @param text
 *   String containing the text to parse.
 * @param editorDivSelector
 *   String the selector of the current editor.
 *
 * @return String
 *   The parsed text.
 */
function CQ_ParseText(text, editorDivSelector) {
  var form_id = jQuery(editorDivSelector).parents('form').find('[name=form_build_id]')[0];
  var target_url = Drupal.settings.basePath + 'closedquestion/parsecontentjs';
  var result = jQuery.ajax({
    url: target_url,
    data: {
      form_build_id: form_id.value,
      parse_content: text
    },
    async: false
  }).responseText;
  var obj = jQuery.parseJSON(result);
  return obj.data;
}

/**
 * Loads a template into the editor. Gives a warning if the editor is not empty.
 *
 * @param template
 *   The XML string to load.
 */
function CQ_LoadTemplate(template) {
  var lang = Drupal.settings.closedquestion.language;
  var bodyField = CQ_FindBodyField(lang);
  var confirmed = true;
  if (jQuery(bodyField)[0].value.length > 0) {
    confirmed = confirm("Loading this template will overwrite your current question. Are you sure?");
  }
  if (confirmed) {
    jQuery(bodyField)[0].value = template;
    CQ_ShowTree(true);
  }
}


/**
 * Helper, converts Media module tag to <img>
 * @param {string} mediaTag
 * @returns {string}
 */
function CQ_ImgWebsitelinkConvert(html, toAnchors) {
  let returnHTML = '';

  if (toAnchors === false || typeof toAnchors === 'undefined') {
    // Convert a-tags to custom websitelinks
    let $html = jQuery('<div>' + html + '</div>');
    jQuery('a', $html).replaceWith(function () {
      let $websitelink = jQuery('<websitelink />');
      let $a = jQuery(this);
      let aHTML = $a.html();
      let attributes = $a.prop("attributes");

      $websitelink.attr('value', aHTML);
      $websitelink.html(aHTML);
      jQuery.each(attributes, function () {
        $websitelink.attr(this.name, this.value);
      });
      return $websitelink;
    });

    returnHTML = $html.html();
  }
  else {
    // Replace custom websitelink tags with a-tags.
    let $html = jQuery('<div></div>').append(html);
    jQuery('websitelink', $html).replaceWith(function () {
      let $a = jQuery('<a />');
      let $websitelink = jQuery(this);
      let attributes = $websitelink.prop("attributes");
      $a.html($websitelink.attr('value'));
      jQuery.each(attributes, function () {
        if (this.name !== 'value') {
          $a.attr(this.name, this.value);
        }
      });

      return $a;
    });
    returnHTML = $html.html();
  }

  return returnHTML;
}


/**
 * Helper, converts Media module tag to <img>
 * @param {string} mediaTag
 * @returns {string}
 */
function CQ_ImgHtmlFromMediatag(mediaTag) {
  var mediaTagCopy = mediaTag;
  var mediaObj, $img;

  /* Try to parse media tag. */
  mediaTag = mediaTag.replace('[[', '').replace(']]', '');
  try {
    mediaObj = JSON.parse(mediaTag);
  }
  catch (err) {
    mediaObj = false;
  }

  if (mediaObj !== false) {
    /* Obtain html to return. */
    $img = jQuery('<img />');

    if (mediaObj.attributes && mediaObj.attributes['data-src']) {
      $img.attr('src', mediaObj.attributes['data-src']);
    }
    else {
      $img.attr('src', Drupal.settings.basePath + 'closedquestion/getmedia/' + mediaObj.fid);
    }

    $img.attr('data-fid', mediaObj.fid);
    $img.attr('data-view-mode', mediaObj.view_mode);

    if (mediaObj.attributes) {
      $img.attr(mediaObj.attributes);
    }

    $img.addClass('media-image');

    return $img[0].outerHTML;
  }

  /* Return original tag. */
  return mediaTagCopy;
}


/**
 * Helper, returns Media tag for image element.
 * @param {object} $image
 *   A jQuery <img> element.
 * @returns {String}
 *   The media tag.
 */
function CQ_MediatagFromImg($image) {
  var attributes = {};
  var styleAttrValues = [];
  if ($image.width() > 0) {
    styleAttrValues.push('width:' + $image.width() + 'px');
    attributes['width'] = $image.width();
  }

  if ($image.height() > 0) {
    styleAttrValues.push('height:' + $image.height() + 'px');
    attributes['height'] = $image.height();
  }

  attributes['typeof'] = "foaf:Image";
  attributes['class'] = $image.attr('class');
  attributes['style'] = styleAttrValues.join(';');
  attributes['data-src'] = $image.attr('src');
  attributes['title'] = $image.attr('title');
  attributes['alt'] = $image.attr('alt');

  var tagContent = {
    "fid": $image.attr('data-fid'),
    "type": 'media',
    "view_mode": $image.attr('data-view-mode')
  };

  tagContent['attributes'] = attributes;

  return '[[' + JSON.stringify(tagContent) + ']]';
}


/**
 * Helper function. Detects whether Media Module 2.x is enabled.
 * @returns {Boolean}
 */
function CQ_MediaModule2IsEnabled() {
  return (typeof Drupal.media !== 'undefined' && typeof Drupal.media.popups !== 'undefined' && typeof Drupal.media.popups.mediaBrowser !== 'undefined');
}

Drupal.behaviors.closedQuestionEditor = {
  attach: function (context) {
    setTimeout(function () {
      CQ_InitialView(context)
    }, 1); // Timeout is need for correct init of jstree.
  }
};
