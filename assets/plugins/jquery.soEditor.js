/**
 * soEditor
 * based on boilerplate version 1.3
 * @param {object} $ A jQuery object
 **/
(function ($) {
  "use strict"; //ECMA5 strict modus

  $.soEditor = function (element, settings) {
    /* define vars
     */

    /* this object will be exposed to other objects */
    var publicObj = this;

    //the version number of the plugin
    publicObj.version = "1.0";

    /* this object holds functions used by the plugin boilerplate */
    var _helper = {
      /**
       * Call hooks, additinal parameters will be passed on to registered plugins
       * @param {string} name
       */
      doHook: function (name) {
        var i;
        var pluginFunctionArgs = [];

        /* remove first two arguments */
        for (i = 1; i < arguments.length; i++) {
          pluginFunctionArgs.push(arguments[i]);
        }

        /* call plugin functions */
        if (_globals.plugins !== undefined) {
          /* call plugins */
          $.each(_globals.plugins, function (soEditor, extPlugin) {
            if (
              extPlugin.__hooks !== undefined &&
              extPlugin.__hooks[name] !== undefined
              ) {
              extPlugin.__hooks[name].apply(publicObj, pluginFunctionArgs);
            }
          });
        }

        /* trigger event on main element */
        _globals.$element.trigger(name, pluginFunctionArgs);
      },
      /**
       * Initializes the plugin
       */
      doInit: function () {
        _helper.doHook(
          "soEditor.beforeInit",
          publicObj,
          element,
          settings
          );
        publicObj.init();
        _helper.doHook("soEditor.init", publicObj);
      },
      /**
       * Loads an external script
       * @param {string} libName
       * @param {string} errorMessage
       */
      loadScript: function (libName, errorMessage) {
        /* remember libname */
        _cdnFilesToBeLoaded.push(libName);

        /* load script */
        $.ajax({
          type: "GET",
          url: _globals.dependencies[libName].cdnUrl,
          success: function () {
            /* forget libname */
            _cdnFilesToBeLoaded.splice(_cdnFilesToBeLoaded.indexOf(libName), 1); //remove element from _cdnFilesToBeLoaded array

            /* call init function when all scripts are loaded */
            if (_cdnFilesToBeLoaded.length === 0) {
              _helper.doInit();
            }
          },
          fail: function () {
            console.error(errorMessage);
          },
          dataType: "script",
          cache: "cache"
        });
      },
      /**
       * Checks dependencies based on the _globals.dependencies object
       * @returns {boolean}
       */
      checkDependencies: function () {
        var dependenciesPresent = true;
        for (var libName in _globals.dependencies) {
          var errorMessage =
            "jquery.soEditor: Library " +
            libName +
            " not found! This may give unexpected results or errors.";
          var doesExist = $.isFunction(_globals.dependencies[libName])
            ? _globals.dependencies[libName]
            : _globals.dependencies[libName].doesExist;
          if (doesExist.call() === false) {
            if (
              $.isFunction(_globals.dependencies[libName]) === false &&
              _globals.dependencies[libName].cdnUrl !== undefined
              ) {
              /* cdn url provided: Load script from external source */
              _helper.loadScript(libName, errorMessage);
            }
            else {
              console.error(errorMessage);
              dependenciesPresent = false;
            }
          }
        }
        return dependenciesPresent;
      }
    };
    /* keeps track of external libs loaded via their CDN */
    var _cdnFilesToBeLoaded = [];

    /* this object holds all global variables */
    var _globals = {};

    /* handle settings */
    var defaultSettings = {
      "input": $(''),
      "question_options": [],
      "allowDuplicates": false,
      "horizontalAlignment": false,
      "onlyOrder": -1
    };

    _globals.settings = {};

    if ($.isPlainObject(settings) === true) {
      _globals.settings = $.extend(true, {}, defaultSettings, settings);
    }
    else {
      _globals.settings = defaultSettings;
    }

    /* this object contains a number of functions to test for dependencies,
     * doesExist function should return TRUE if the library/browser/etc is present
     */
    _globals.dependencies = {
      /* check for jQuery 1.6+ to be present */
      "jquery1.6+": {
        doesExist: function () {
          var jqv, jqv_main, jqv_sub;
          if (window.jQuery) {
            jqv = jQuery().jquery.split(".");
            jqv_main = parseInt(jqv[0], 10);
            jqv_sub = parseInt(jqv[1], 10);
            if (jqv_main > 1 || (jqv_main === 1 && jqv_sub >= 6)) {
              return true;
            }
          }
          return false;
        },
        cdnUrl: "http://code.jquery.com/jquery-git1.js"
      },
      "jquery.ui.sortable": {
        doesExist: function () {
          return (jQuery.ui && jQuery.ui.sortable);
        }
      }
    };
    _helper.checkDependencies();

    //this object holds all plugins
    _globals.plugins = {};

    /* register DOM elements
     * jQuerified elements start with $
     */
    _globals.$element = $(element);
    _globals.$attributeWrapper = _globals.$element.closest('.xmlJsonEditor_attribute_container');

    _globals.template = '<div class="soEditor_source soEditor_box"><span class="soEditor_source_title">Available items:</span><ul class="soEditor_sortable" id="soEditor_sortable_source"></ul></div><div class="soEditor_target soEditor_box"></div>';

    /**
     * Init function
     **/
    publicObj.init = function () {
      _globals.$element.append(_globals.template);
      _globals.$sourceSortable = _globals.$element.find('#soEditor_sortable_source');
      _globals.$targetSortables = [];

      _globals.$sourceBox = _globals.$element.find('.soEditor_source.soEditor_box');
      _globals.$targetBox = _globals.$element.find('.soEditor_target.soEditor_box');
      _globals.$input = $(cfg('input'));
      _globals.$matchTypeSelector = $('<select style="display:block;margin-bottom:1em;"><option value="exact">' + Drupal.t('This is the complete answer.') + '</option><option value="part">' + Drupal.t('This is part of the answer.') + '</option></select>');

      /* Find out which items are double (have the same content), so that we can show this to end user. */
      var qo_labels = [];
      $.each(cfg('question_options'), function (i) {
        var previous_i = qo_labels.indexOf(this.label);

        if (previous_i >= 0) {
          cfg('question_options')[previous_i].double = true;
          cfg('question_options')[i].double = true;
        }
        qo_labels.push(this.label);
      });

      /* Add options and target lists */
      $.each(cfg('question_options'), function () {
        if (!this.id) {
          disableEditor();
        }
        if (this.id.match('[a-zA-Z]')) {
          _globals.$sourceSortable.append(getOptionElement(this.id, this.label, '', this.double));
        }
        else if (this.id.match('[0-9]')) {
          /* Targets lists, for backwards compatibility only, replaced by <target> */
          var $ul = $('<ul class="soEditor_sortable soEditor_target_sortable" data-id="soEditor_' + this.id + '"></ul>');
          _globals.$targetBox.append('<span>' + formatHTML(this.label) + '</span>');
          _globals.$targetBox.append($ul);
        }
      });

      /* Add targets */
      $.each(cfg('question_targets'), function () {
        if (!this.id) {
          disableEditor();
        }

        var $ul = $('<ul class="soEditor_sortable soEditor_target_sortable" data-id="soEditor_' + this.id + '"></ul>');
        _globals.$targetBox.append('<span>' + formatHTML(this.label) + '</span>');
        _globals.$targetBox.append($ul);
      });


      // Make sure there is always one target box.
      if (_globals.$element.find('.soEditor_target_sortable').length === 0) {
        var $ul = $('<ul class="soEditor_sortable soEditor_target_sortable" data-id="soEditor"></ul>');
        _globals.$targetBox.append('<span class="soEditor_target_title">Selected items:</span>');
        _globals.$targetBox.append($ul);
      }

      // Define regular expression items.
      _globals.specialItems = {
        "__dot__": {"re": '.', "label": "One arbitrary item"},
        "__dot____star__": {"re": '.*', "label": "Zero or more arbitrary items"},
        "__star__": {"re": '*', "label": "Repeat previous item zero or more times", "hiddenByDefault": true}
      };

      _globals.idToRe = $.extend({}, _globals.specialItems);

      _globals.reToId = {};
      $.each(_globals.idToRe, function (key, config) {
        config = $.extend({}, config);
        var re = config.re;
        delete config.re;
        config.id = key;
        _globals.reToId[re] = config;
      });

      // Add regular expression items to list.
      $.each(_globals.idToRe, function (key, config) {
        var hiddenClass = (config.hiddenByDefault === true || cfg('allowSpecialElements') === false) ? ' soEditor_hiddenByDefault' : '';
        _globals.$sourceSortable.append(getOptionElement(key, config.label, 'soEditor_special' + hiddenClass));
      });

      // Provide for only order questions
      if (cfg('onlyOrder') === 1) {
        if (_globals.$input.val() === '') {
          $(".soEditor_target_sortable").append(_globals.$sourceSortable.children().not('.soEditor_special'));
          updateAnswerInput();
        }
        _globals.$sourceSortable.addClass('soEditor_only_order');
        $('.soEditor_source_title').html(Drupal.t('Irrelevant options:'));
        $('.soEditor_target_title').html(Drupal.t('Answer'));
      }

      // Provide for horizontal questions
      if (cfg('horizontalAlignment') === true) {
        _globals.$element.addClass('soEditor_horizontal');

        // Give all dropables the same height in case of horizontal mode.
        var minHeight = 0;

        $('.soEditor_option', _globals.$element).each(function () {
          var thisHeight = $(this).height();
          minHeight = minHeight < thisHeight ? thisHeight : minHeight;
        });
        $('.soEditor_sortable', _globals.$element).css('min-height', minHeight);
      }

      // Init the drag/drop functionality.
      var copyHelper;
      var overSortHandler = function (event, ui) {
        var height, width;
        if (cfg('allowDuplicates')) {
          height = ui.helper.height();
          width = ui.helper.width();
        }
        else {
          height = ui.item.height();
          width = ui.item.width();
        }
        // Show placeholder with same dimensions in target.
        $(this).find('.ui-sortable-placeholder').css({
          "height": height,
          "width": width
        });
      };
      _globals.$sourceSortable.sortable({
        "connectWith": ".soEditor_sortable",
        "forcePlaceholderSize": false,
        "helper": function (e, li) {
          if (li.hasClass('soEditor_special') || cfg('allowDuplicates')) {
            copyHelper = li.clone().insertAfter(li);
            return li.clone();
          }
          return li;
        },
        "over": overSortHandler,
        "sort": overSortHandler,
        "stop": function () {
          if (copyHelper) {
            copyHelper.remove();
          }
          formatAvailableItemsSortable();
          formatTargetItemsSortable();
          updateAnswerInput();
        }
      });

      $(".soEditor_target_sortable").sortable({
        "connectWith": ".soEditor_sortable",
        "over": overSortHandler,
        "sort": overSortHandler,
        "receive": function () {
          copyHelper = null;
        },
        "stop": function () {
          formatAvailableItemsSortable();
          formatTargetItemsSortable();
          updateAnswerInput();
        }
      });

      // Keep input and editor in sync
      _globals.$input.on('keyup paste', function () {
        window.clearTimeout(_globals.$input.data('soEditor_timeout'));
        var to = window.setTimeout(function () {
          updateQuestionGUI();
        }, 1000);
        _globals.$input.data('soEditor_timeout', to);
      });

      // Hide preset select.
      $('#xmlEditor_presets_select', _globals.$element.parent()).hide();

      // Update the GUI.
      updateQuestionGUI();

      if (_globals.$input.val().trim() === '') {
        // Make sure the input always contains a match.
        updateAnswerInput();
      }
    };

    /**
     * Disables the editor.
     *
     * @param {string} errorMessage
     *   The error message. When left empty, a default is shown.
     */
    function disableEditor(errorMessage) {
      errorMessage = errorMessage ? errorMessage : 'This advanced condition is not (yet) supported by the editor. The editor might not display the condition correctly. Please use below input.';
      $('#soEditor_feedback').remove();

      _globals.$element.addClass('xmlEditorAttributeFeedback_error');
      _globals.$element.after('<p id="soEditor_feedback" class="xmlJsonEditor_feedback_description" style="color:red;">' + errorMessage + '</p>');

      $('.xmlEditorAttributeFeedback', _globals.$attributeWrapper).addClass('soEditor_visible');
      $('#xmlEditor_presets_select', _globals.$attributeWrapper).addClass('soEditor_visible');
      _globals.$input.addClass('soEditor_visible');
    }

    /**
     * Enables the editor.
     */
    function enableEditor() {
      $('#soEditor_feedback').remove();
      _globals.$element.removeClass('xmlEditorAttributeFeedback_error');
      $('.xmlEditorAttributeFeedback', _globals.$attributeWrapper).removeClass('soEditor_visible');
      $('#xmlEditor_presets_select', _globals.$attributeWrapper).removeClass('soEditor_visible');
      _globals.$input.removeClass('soEditor_visible');
    }

    /**
     * Creates an option jQuery DOM element.
     *
     * @param {string} dataId
     * @param {string} label
     * @param {string} classAttr
     * @param {boolean} addTitle
     * @returns {object}
     */
    function getOptionElement(dataId, label, classAttr, addTitle) {
      classAttr = classAttr ? ' ' + classAttr : '';
      let title = (addTitle === true ? "Option internal id: " + dataId : '');
      return $('<li title="' + title + '" data-id="soEditor_' + dataId + '" class="soEditor_option' + classAttr + '">' + formatHTML(label) + '</li>');
    }

    /**
     * Updates the answer input based on the question GUI.
     */
    function updateAnswerInput() {
      let re_items_raw = [];
      let re_items = [];

      //Collect regular expression items.
      _globals.$element.find('.soEditor_target_sortable').each(function () {
        let $target_sortable = $(this);
        if ($target_sortable.attr('data-id') !== undefined) {
          re_items_raw.push($target_sortable.attr('data-id'));
        }
        $target_sortable.find('li.soEditor_option').each(function () {
          re_items_raw.push($(this).attr('data-id'));
        });
      });

      if (cfg('onlyOrder') === 2) {
        /* 1. Deal with 'Only select, order of options does not matter' */
        let currentTargetListId = '';
        let tl_re_items = {};     // This will hold the items per target box.
        let targetListIds = []; // This will remember the order of the target lists.

        // Get items per target list.
        $.each(re_items_raw, function (i, re_item) {
          re_item = re_item.replace(/soEditor_?/g, '');
          tl_re_items[currentTargetListId] = $.isArray(tl_re_items[currentTargetListId]) ? tl_re_items[currentTargetListId] : [];
          let isSpecialItem = typeof _globals.specialItems[re_item] !== 'undefined';

          // Change target?
          if (re_item.match('[0-9]')) {
            currentTargetListId = re_item;
            targetListIds.push(currentTargetListId);
            return;
          }
          else {
            // Get items per target.
            if (_globals.idToRe[re_item]) {
              re_item = _globals.idToRe[re_item].re;
            }
            if (re_item !== '' && (cfg('allowDuplicates') === true || tl_re_items[currentTargetListId].indexOf(re_item) < 0 || isSpecialItem)) {
              tl_re_items[currentTargetListId].push(re_item);
            }
          }
        });

        if (targetListIds.length === 0) {
          // No target lists defined, add default.
          targetListIds.push("");
        }

        // Create regular expression. The answer will be ordered alphabetically per target before processing.
        // see CqQuestionSelectorOrder.class.php::getAnswerForChoice
        //
        // To match '0aac1bc' the following expression will be created        //
        //
        // ^(0)(?=[a-zA-Z]{4}1)(?=[^1]*a{2})(?=[^1]*c{1}((.*)(1)(?=[a-zA-Z]{2}$)(?=[^b]*b{1})(?=[^c]*c{1})(.*)$
        //
        // Per target list:
        // 1) 0 wil check whether the target list is there, in this case: target list with id 0
        // 2) (?=[a-zA-Z]{4}1) will count the number of items until the next target, in this case: 4
        // 3) (?=[^1]*a{2}) will check if a particular item exists in the right number; in this case 'a' should exist 2 times
        // 4) .* will just match everything until the next target list.
        //
        // For the last target list, minor variants are created. Redundant parentheses are added to ease parsing in updateQuestionGUI.
        $.each(targetListIds, function (index, targetListId) {
          let items = typeof tl_re_items[targetListId] === 'undefined' ? [] : tl_re_items[targetListId];
          let dotZeroIncluded = false;

          // Filter out duplicate '.*'
          items = items.filter(function (el) {
            if (el === '.*' && dotZeroIncluded) {
              return false;
            }
            dotZeroIncluded = el === '.*' ? true : dotZeroIncluded;
            return true;
          });

          let isLastTarget = (targetListIds.indexOf(targetListId) + 1 === targetListIds.length);
          let numberOfItems = items.length;
          let nextTargetListElement = isLastTarget ? '$' : targetListIds[index + 1];
          let negatedElement, j;
          let itemIsProcessed = {};

          /* 1) Add target list id */
          if (targetListId !== '') {
            re_items.push('(' + targetListId + ')');
          }

          /* 2) Check for the correct number of items within the target (if no 'zero or more arbitrary items' selected) */
          if (items.indexOf('.*') === -1) {
            // add 'target item count'
            re_items.push('(?=[a-zA-Z]{' + numberOfItems + '}' + nextTargetListElement + ')');
          }
          else if (items.indexOf('.') !== -1) {
            // 'zero or more arbitrary items' selected, but also 'one arbitrary item':
            // add different 'target item count', checking for minimum number of
            // selected options.
            re_items.push('(?=[a-zA-Z]{' + (numberOfItems - 1) + ',}' + nextTargetListElement + ')');
          }

          /* 3) Handle items */
          $.each(items, function (i, item) {
            if (itemIsProcessed[item] === true || item === '.' || item === '.*') {
              return;
            }

            // Count the number of items of this type
            let numberOfIdenticalItems = 0;
            $.each(items, function (i, count_item) {
              if (item === count_item) {
                numberOfIdenticalItems++;
              }
            });

            negatedElement = isLastTarget ? item : nextTargetListElement;
            re_items.push('(?=[^' + negatedElement + ']*' + item + '{' + numberOfIdenticalItems + '})');
            itemIsProcessed[item] = true;
          });

          // Add wildcard
          if (items.length > 0) {
            re_items.push('(.*)');
          }
        });
      }
      else {
        /* 2. Deal with 'Select & order' and 'Only order': get a flat list of reg exp items. */
        $.each(re_items_raw, function (i, re_item) {
          re_item = re_item.replace(/soEditor_?/g, '');
          if (_globals.idToRe[re_item] !== undefined) {
            re_item = _globals.idToRe[re_item].re;
          }
          re_items[i] = re_item;
        });
      }

      // Fill input
      _globals.$input.val('^' + re_items.join('') + '$');

      // Keep input and editor in sync
      _globals.$input.trigger('change');
    }

    /**
     * Updates the question GUI based on the value of the input.
     */
    function updateQuestionGUI() {
      /* Process regular expression */
      var val = _globals.$input.val(), new_val = '', i;
      var re_items_raw;
      var re_items = [];
      var shouldDisableEditor = false;
      var legacyRegExpFound = false;

      if (cfg('onlyOrder') === 2) {
        /* 1. Deal with 'Only select, order of options does not matter'
         *
         * We should simplify the 'val', so that we can easily add reg exp items
         *
         * See updateAnswerInput for the generated regular expression.
         */
        if (val.search(/\([a-zA-z.*]*\)\|/) >= 0 || val.search(/\(\([a-zA-z.*]\)\)/) >= 0) {
          // Legacy regular expression val consisting of anagrams.
          val = _simplifyLegacyOnlySelectVal(val);
          legacyRegExpFound = true;
        }
        else {
          // New regular expression.
          val = _simplifyOnlySelectVal(val);
        }
      }

      /* 2. Get a flat list of reg exp items. */
      re_items_raw = val.split('');
      $.each(re_items_raw, function (i, re_item) {
        var removed_item;
        if (_globals.reToId[re_item] !== undefined) {
          re_item = _globals.reToId[re_item].id;
        }

        switch (re_item) {
          case '__negate__':
            if (i > 0) {
              re_items.push(re_item);
            }
            break;

          case '$':
            break;

          case '__star__':
            // Replace '__dot__'-element followed by '__star__'-element
            // by '__dot____star__'-element
            if (re_items[re_items.length - 1] === '__dot__') {
              removed_item = re_items.pop();
              re_items.push(removed_item + re_item);
            }
            break;

          case '__opencurlybrace__':
          case '__closecurlybrace__':
          case '__opensquarebracket__':
          case '__closequarebracket__':
            shouldDisableEditor = true;
            break;

          default:
            re_items.push(re_item);
            break;
        }
      });

      /* Update question */

      // Get a list with unique items in the 'available items' box
      _globals.$sourceSortable.append($(".soEditor_target_sortable").children());
      formatAvailableItemsSortable();

      var $target = $('.soEditor_target_sortable').eq(0);
      $.each(re_items, function (i, re_item) {
        // Change target?
        if (re_item.match('[0-9]')) {
          $target = $('[data-id="soEditor_' + re_item + '"]');
          return;
        }

        // Prevent all strange DOM ids from being generated.
        if (cfg('onlyOrder') !== 2 && re_item.match('[^a-zA-z0-9]')) {
          shouldDisableEditor = true;
        }
        else if (re_item.match('[^a-zA-z0-9_\(\)\|]')) { // Only select can contain more items.
          shouldDisableEditor = true;
          return;
        }

        // Move item.
        var $move_item = $('[data-id="soEditor_' + re_item + '"]', _globals.$sourceSortable);

        if ($move_item.hasClass('soEditor_special') || cfg('allowDuplicates')) {
          $move_item = $move_item.clone(); // Clone if special item or allowDuplicates
        }

        // Append item to target
        $target.append($move_item);
      });

      if (shouldDisableEditor === true) {
        disableEditor();
      }
      else {
        enableEditor();
      }

      if (legacyRegExpFound === true) {
        // Make sure the input receives the new reg exp.
        updateAnswerInput();
      }

      formatTargetItemsSortable();
    }

    /**
     * Returns a setting
     *
     * @param {string} paramName
     * @returns {mixed}
     */
    function cfg(paramName) {
      var returnVal;
      if (paramName === 'onlyOrder') {
        returnVal = parseInt(_globals.settings.onlyOrder, 10);
        returnVal = isNaN(returnVal) ? -1 : returnVal;
      }
      else {
        returnVal = _globals.settings[paramName];
      }

      return returnVal;
    }

    /**
     * Handles  'only select' matches
     *
     * @param {string} val
     *   The anagram value
     * @returns {string}
     *   The simplified value, which can be used tot update the GUI.
     */
    function _simplifyOnlySelectVal(val) {
      var new_val = '';
      var current_target = "";
      var items_in_current_target_counted = null;
      var items_in_current_target_set = null;
      var count_difference;
      var current_target_zero_or_more = false;
      var i;

      val.split('(').filter(function (str) {
        if (str.search(/\d\)/) === 0 || str.search(/\$$/) >= 0) {
          /* New target found or end of string */
          count_difference = items_in_current_target_set - items_in_current_target_counted;

          // 1. Check if we should add '.' items.
          if (items_in_current_target_set > 0 && count_difference > 0) {
            for (i = 0; i < count_difference; i++) {
              new_val += '.';
            }
          }

          // 2. Check if we should add '.*' items.
          if (str.search(/\.\*\)\$/) === 0 && items_in_current_target_set === null) {
            // Last item of last target contains .*
            current_target_zero_or_more = true;
          }

          if ((isNaN(count_difference) && items_in_current_target_counted > 0) || current_target_zero_or_more) {
            new_val += '.*';
          }
        }

        if (str.search(/\d\)/) === 0) {
          /* New target found: Add it to string */
          current_target = parseInt(str.replace(')', ''), 10);
          new_val += current_target;

          // Reset some counters/flags
          items_in_current_target_set = null;
          items_in_current_target_counted = null;
          current_target_zero_or_more = false;
        }

        if (str.search(/\?\=\[\^(\d|[a-zA-z])\]\*([a-zA-Z])\{(\d*)\}/ig) === 0) {
          var match_result = str.match(/\*([a-zA-Z])\{(\d*)/);
          if (match_result[1] && match_result[2]) {
            // New item found: Add it to string; match_result[2] holds the number of instances of this item.
            for (i = 0; i < parseInt(match_result[2], 10); i++) {
              new_val += match_result[1];
            }
            items_in_current_target_counted = items_in_current_target_counted + parseInt(match_result[2], 10);
          }
        }

        if (str.search(/\?\=\[[^\]]*\]{(\d*),?}/g) === 0) {
          // New 'target item count' found: Remember it so we can add 'one arbitrary item' items.
          var match_result = str.match(/{(\d*),?}/);
          if (match_result[1]) {
            items_in_current_target_set = parseInt(match_result[1], 10);
            if (str.match(/{(\d*),}/)) {
              current_target_zero_or_more = true;
            }
          }
        }

        if (str.search(/\.\*\)/) === 0 && items_in_current_target_set === null) {
          current_target_zero_or_more = true;
        }
      });

      return new_val;
    }

    /**
     * Handles legacy 'only select' matches, containing anagrams of possible
     *   answers.
     *
     * @param {string} val
     *   The anagram value
     * @returns {string}
     *   The simplified value, which can be used tot update the GUI.
     */
    function _simplifyLegacyOnlySelectVal(val) {
      var re_items_raw;
      var re_targets;

      // Get items per target.
      re_items_raw = val.split(/[\d]/).filter(function (str) {
        return (str === "" || str === "^" || str === "$") ? false : true;
      });

      // Get target ids.
      re_targets = val.split(/[^\d]/).filter(function (str) {
        return str === "" ? false : true;
      });
      if (re_targets.length === 0) {
        re_targets.push(""); // In case there is 1 target, fake its id.
      }

      // Rebuild val with unique items per target box.
      val = "";
      $.each(re_items_raw, function (i, re_item_raw) {
        var re_item = re_item_raw.split("|")[0];
        val += re_targets[i] + re_item;
      });

      return val;
    }

    /**
     * Remove duplicates from a list.
     *
     * @param {object} $ul
     *   A jQuery DOM list object.
     *
     * @param {boolean} ignoreSpecialItems
     *   If true, special items are not taken into account. Default: false.
     */
    function sortableUniqueItems($ul, ignoreSpecialItems) {
      ignoreSpecialItems = typeof ignoreSpecialItems === 'undefined' ? false : ignoreSpecialItems;
      var seen = {};
      $ul.find('li.soEditor_option').each(function () {
        // Remove duplicates.
        var $li = $(this);
        var id = $li.attr('data-id');
        var re_item = id.replace(/soEditor_?/g, '');
        var isSpecialItem = typeof _globals.specialItems[re_item] !== 'undefined';

        if (seen[id] === true && ((isSpecialItem === true && ignoreSpecialItems === false) || isSpecialItem === false)) {
          $li.remove();
        }
        else {
          seen[id] = true;
        }
      });
    }

    /**
     * Format 'available items' sortable.
     */
    function formatAvailableItemsSortable() {
      // Make sure sortable has unique items.
      sortableUniqueItems(_globals.$sourceSortable);

      // Unset margin left set by formatTargetItemsSortable.
      _globals.$sourceSortable.find('li.soEditor_option').css({"margin-left": ''});
    }

    /**
     * Format target item sortables.
     */
    function formatTargetItemsSortable() {
      // Deal with 'Only select, order of options does not matter'
      if (cfg('onlyOrder') === 2 && cfg('allowDuplicates') === false) {
        $(".soEditor_target_sortable").each(function () {
          sortableUniqueItems($(this), true);  // Make sure each target sortable has unique items.
        });
      }
    }

    /**
     * Formats HTML
     *
     * @param {string} html
     * @returns {string}
     *   The formatted contents.
     */
    function formatHTML(html) {
      /* Handle Media tags */
      var inlineTag = '', imgHTML = 'img', mediaObj, i;
      var html, styleAttr;
      var matches = html.match(/\[\[.*?\]\]/g);
      if (matches) {
        for (i = 0; i < matches.length; i++) {
          inlineTag = matches[i];
          inlineTag = inlineTag.replace('[[', '').replace(']]', '');
          mediaObj = JSON.parse(inlineTag);
          if (mediaObj && mediaObj.attributes && (mediaObj.attributes['data-src'] || mediaObj.attributes['src'])) {
            mediaObj.attributes['data-src'] = mediaObj.attributes['data-src'] ? mediaObj.attributes['data-src'] : mediaObj.attributes['src'];
            styleAttr = mediaObj.attributes.style ? ' style="' + mediaObj.attributes.style + '"' : '';
            imgHTML = '<img src="' + mediaObj.attributes['data-src'] + '" width="' + mediaObj.attributes['width'] + '" height="' + mediaObj.attributes['height'] + '"' + styleAttr + ' />';
          }
          html = html.replace(matches[i], imgHTML);
        }
      }
      return html;
    }

    /* initialize soEditor
     */
    if (_cdnFilesToBeLoaded.length === 0) {
      _helper.doInit();
    }
  }
  ;

  $.fn.soEditor = function (settings) {
    return this.each(function () {
      if (undefined === $(this).data("soEditor")) {
        var plugin = new $.soEditor(this, settings);
        $(this).data("soEditor", plugin);
      }
    });
  };
})(jQuery);
