/**
 * simpleTableEdit
 * based on boilerplate version 1.3
 * @param {object} $ A jQuery object
 **/
(function ($) {
  "use strict"; //ECMA5 strict modus

  $.simpleTableEdit = function (element, settings) {
    /* define vars
     */

    /* this object will be exposed to other objects */
    var publicObj = this;

    //the version number of the plugin
    publicObj.version = "1.0";

    /* this object holds functions used by the plugin boilerplate */
    var _helper = {
      /**
       * Call hooks, additinal parameters will be passed on to registered plugins
       * @param {string} name
       */
      doHook: function (name) {
        var i;
        var pluginFunctionArgs = [];

        /* remove first two arguments */
        for (i = 1; i < arguments.length; i++) {
          pluginFunctionArgs.push(arguments[i]);
        }

        /* call plugin functions */
        if (_globals.plugins !== undefined) {
          /* call plugins */
          $.each(_globals.plugins, function (simpleTableEdit, extPlugin) {
            if (
              extPlugin.__hooks !== undefined &&
              extPlugin.__hooks[name] !== undefined
              ) {
              extPlugin.__hooks[name].apply(publicObj, pluginFunctionArgs);
            }
          });
        }

        /* trigger event on main element */
        _globals.$element.trigger(name, pluginFunctionArgs);
      },
      /**
       * Initializes the plugin
       */
      doInit: function () {
        _helper.doHook(
          "simpleTableEdit.beforeInit",
          publicObj,
          element,
          settings
          );
        publicObj.init();
        _helper.doHook("simpleTableEdit.init", publicObj);
      },
      /**
       * Loads an external script
       * @param {string} libName
       * @param {string} errorMessage
       */
      loadScript: function (libName, errorMessage) {
        /* remember libname */
        _cdnFilesToBeLoaded.push(libName);

        /* load script */
        $.ajax({
          type: "GET",
          url: _globals.dependencies[libName].cdnUrl,
          success: function () {
            /* forget libname */
            _cdnFilesToBeLoaded.splice(_cdnFilesToBeLoaded.indexOf(libName), 1); //remove element from _cdnFilesToBeLoaded array

            /* call init function when all scripts are loaded */
            if (_cdnFilesToBeLoaded.length === 0) {
              _helper.doInit();
            }
          },
          fail: function () {
            console.error(errorMessage);
          },
          dataType: "script",
          cache: "cache"
        });
      },
      /**
       * Registers a plugin
       * @param {string} name Name of plugin, must be unique
       * @param {object} object An object {("functions": {},) (, "hooks: {})}
       */
      registerPlugin: function (name, object) {
        var plugin;
        var hooks;

        /* reorder plugin */
        hooks = $.extend(true, {}, object.hooks);
        plugin = object.functions !== undefined ? object.functions : {};
        plugin.__hooks = hooks;

        /* add plugin */
        _globals.plugins[name] = plugin;
      },
      /**
       * Calls a plugin function, all additional arguments will be passed on
       * @param {string} simpleTableEdit
       * @param {string} pluginFunctionName
       */
      callPluginFunction: function (simpleTableEdit, pluginFunctionName) {
        var i;

        /* remove first two arguments */
        var pluginFunctionArgs = [];
        for (i = 2; i < arguments.length; i++) {
          pluginFunctionArgs.push(arguments[i]);
        }

        /* call function */
        _globals.plugins[simpleTableEdit][pluginFunctionName].apply(
          null,
          pluginFunctionArgs
          );
      },
      /**
       * Checks dependencies based on the _globals.dependencies object
       * @returns {boolean}
       */
      checkDependencies: function () {
        var dependenciesPresent = true;
        for (var libName in _globals.dependencies) {
          var errorMessage =
            "jquery.simpleTableEdit: Library " +
            libName +
            " not found! This may give unexpected results or errors.";
          var doesExist = $.isFunction(_globals.dependencies[libName])
            ? _globals.dependencies[libName]
            : _globals.dependencies[libName].doesExist;
          if (doesExist.call() === false) {
            if (
              $.isFunction(_globals.dependencies[libName]) === false &&
              _globals.dependencies[libName].cdnUrl !== undefined
              ) {
              /* cdn url provided: Load script from external source */
              _helper.loadScript(libName, errorMessage);
            }
            else {
              console.error(errorMessage);
              dependenciesPresent = false;
            }
          }
        }
        return dependenciesPresent;
      }
    };
    /* keeps track of external libs loaded via their CDN */
    var _cdnFilesToBeLoaded = [];

    /* this object holds all global variables */
    var _globals = {};

    /* handle settings */
    var defaultSettings = {};

    _globals.settings = {};

    if ($.isPlainObject(settings) === true) {
      _globals.settings = $.extend(true, {}, defaultSettings, settings);
    }
    else {
      _globals.settings = defaultSettings;
    }

    /* this object contains a number of functions to test for dependencies,
     * doesExist function should return TRUE if the library/browser/etc is present
     */
    _globals.dependencies = {
      /* check for jQuery 1.6+ to be present */
      "jquery1.6+": {
        doesExist: function () {
          var jqv, jqv_main, jqv_sub;
          if (window.jQuery) {
            jqv = jQuery().jquery.split(".");
            jqv_main = parseInt(jqv[0], 10);
            jqv_sub = parseInt(jqv[1], 10);
            if (jqv_main > 1 || (jqv_main === 1 && jqv_sub >= 6)) {
              return true;
            }
          }
          return false;
        },
        cdnUrl: "http://code.jquery.com/jquery-git1.js"
      }
    };
    _helper.checkDependencies();

    //this object holds all plugins
    _globals.plugins = {};

    /* register DOM elements
     * jQuerified elements start with $
     */
    _globals.$element = $(element);

    // Keeps track of active cell
    _globals.navigationActiveCellPos = {"col": null, "row": null};
    /**
     * Init function
     **/
    publicObj.init = function () {
      preClean();
      resetTablesSelection();

      makeTablesSelectable(true);
      makeTablesCopyable();
      makeTablesNavigationable(true);
    };

    /**
     * Adds a table.
     *
     * @param {object} dimensions
     *   {"cols": int, "rows": int}.
     */
    publicObj.addTable = function (dimensions) {
      var $activeTable = getActiveTable();
      var newTable;
      if ($activeTable.length === 0) {
        newTable = addEmptyTable(dimensions);

        makeTablesSelectable(true);
        makeTablesCopyable();

        return newTable;
      }

    };


    /**
     * Removes active table.
     */
    publicObj.removeTable = function () {
      var $activeTable = getActiveTable();
      if ($activeTable.length === 1) {
        $activeTable.remove();
      }
    };

    /**
     * Adds a row.
     */
    publicObj.addRow = function () {
      var rowCellsInfo = [];
      var $newRow;

      var $activeTable = getActiveTable();
      if ($activeTable.length === 0) {
        // No active table.
        return false;
      }

      var $activeCell = publicObj.getSelectedCells();
      if ($activeCell.length !== 1) {
        // No active cell.
        return false;
      }

      var activeCellInfo = getCellInfo($activeCell);
      var $activeRow;

      // Get all cells in the same row.
      rowCellsInfo = getCellRowCellInfo($activeCell);

      // Sometimes all cells have the same row value and the same rowspan value
      // > 1. In those cases, set the rowspan value of those cells to 1.
      var allCellsSimilar = true;
      $.each(rowCellsInfo, function (i) {
        if (i === 0) {
          return;
        }
        var cellInfo = rowCellsInfo[i];
        var prevCellInfo = rowCellsInfo[i - 1];
        var isSimilar = (cellInfo.row === prevCellInfo.row && cellInfo.next_row === prevCellInfo.next_row);

        allCellsSimilar = (allCellsSimilar && isSimilar) ? true : false;
      });
      if (allCellsSimilar) {
        $.each(rowCellsInfo, function (i) {
          rowCellsInfo[i].cell.attr('rowspan', 1);
        });
      }

      // Get the active row.
      $activeRow = $activeTable.find('tr').eq(activeCellInfo.next_row - 1);

      // Create the new row.
      $newRow = $('<tr></tr>');

      $.each(rowCellsInfo, function (i) {
        var cellInfo = rowCellsInfo[i];
        var nodeName;
        var $newCell;

        if (cellInfo.rowspan > 1 && cellInfo.next_row !== activeCellInfo.next_row) {
          // Increase rowspan of multiple row spanning cells.
          cellInfo.cell.attr('rowspan', cellInfo.rowspan + 1);
        }
        else {
          // Create new cell for other cells.
          nodeName = cellInfo.cell[0].nodeName.toLowerCase();
          $newCell = $('<' + nodeName + '>\u200B</' + nodeName + '>');
          $newRow.append($newCell);
          if (cellInfo.colspan > 1) {
            $newCell.attr('colspan', cellInfo.colspan);
          }
        }
      });

      // Insert it.
      if ($activeRow.length === 1) {
        // Add after active row.
        $activeRow.after($newRow);
      }
      else {
        // Add at end of table.
        $activeTable.append($newRow);
      }

      // Reset table selectability.
      makeTablesSelectable(true);

      return true;
    };

    /**
     * Splits a cell.
     *
     * @param {type} onlyPreview
     *   Whether to only check if the cell can be split.
     *
     * @returns {Boolean}
     *   True if the cell can be split.
     */
    publicObj.splitCell = function (onlyPreview) {
      onlyPreview = typeof onlyPreview === 'undefined' ? false : onlyPreview;
      var $activeTable = getActiveTable();

      if (!$activeTable) {
        return false;
      }

      var $selectedTds = $activeTable.find('.ste_active');

      if ($selectedTds.length !== 1) {
        return false;
      }

      return publicObj.addColumn(onlyPreview, true);
    };

    /**
     * Will attempt to merge selected table cells.
     *
     * @param {type} onlyPreview
     * @returns {undefined|Boolean}
     */
    publicObj.mergeCells = function (onlyPreview) {
      onlyPreview = typeof onlyPreview === 'undefined' ? false : onlyPreview;

      var isHorizontal = false;
      var isVertical = false;
      var i;
      var firstCellRowspan, firstCellColspan;
      var $activeTable = getActiveTable();

      if (!$activeTable) {
        return false;
      }

      var $selectedTds = $activeTable.find('.ste_active');
      var cellsInfo = [];
      if ($selectedTds.length <= 1) {
        return false;
      }

      $selectedTds.each(function () {
        var $cell = $(this);
        cellsInfo.push(getCellInfo($cell));
      });

      // Check if horizontal or vertical selection.
      for (i = 1; i < cellsInfo.length; i++) {
        isHorizontal = (cellsInfo[i].row === cellsInfo[i - 1].row) ? true : isHorizontal;
        isVertical = !(cellsInfo[i].row === cellsInfo[i - 1].row) ? true : isVertical;
      }

      if (isHorizontal && isVertical) {
        // Both horizontal and vertical cells selected.
        return false;
      }

      if (isHorizontal) {
        /* Horizontal selection.
         */

        /* Do checks */
        firstCellRowspan = cellsInfo[0].rowspan;
        for (i = 1; i < cellsInfo.length; i++) {
          if (cellsInfo[i].rowspan !== firstCellRowspan) {
            // There are cells having a different colspan attribute set.
            return false;
          }

          if (cellsInfo[i - 1].cell.next()[0] !== cellsInfo[i].cell[0]) {
            // Cells not adjacent.
            return false;
          }
        }

        /* Merge cells. */
        if (!onlyPreview) {
          firstCellColspan = cellsInfo[0].colspan;

          for (i = 1; i < cellsInfo.length; i++) {
            // Add contents to first cell.
            cellsInfo[0].cell.html(cellsInfo[0].cell.html() + cellsInfo[i].cell.html());

            // Increase first cell colspan.
            firstCellColspan += cellsInfo[i].colspan;

            // Remove the cell.
            cellsInfo[i].cell.remove();
          }

          cellsInfo[0].cell.attr('colspan', firstCellColspan);

          // Reset selection.
          resetTablesSelection();
        }
      }
      else {
        /* Vertical selection.
         */

        // Do checks.
        firstCellColspan = cellsInfo[0].colspan;
        for (i = 1; i < cellsInfo.length; i++) {
          if (cellsInfo[i].colspan !== firstCellColspan) {
            // There are cells having a different colspan attribute set.
            return false;
          }

          if (cellsInfo[i].row !== cellsInfo[i - 1].next_row) {
            // Cells not adjacent.
            return false;
          }
        }

        // Merge cells.
        if (!onlyPreview) {
          firstCellRowspan = cellsInfo[0].rowspan;
          for (i = 1; i < cellsInfo.length; i++) {
            // Add contents to first cell.
            cellsInfo[0].cell.html(cellsInfo[0].cell.html() + cellsInfo[i].cell.html());

            // Increase first cell colspan.
            firstCellRowspan += cellsInfo[i].rowspan;

            // Remove the cell.
            cellsInfo[i].cell.remove();
          }

          cellsInfo[0].cell.attr('rowspan', firstCellRowspan);

          // Reset selection.
//          makeTablesNavigationable(true);
          resetTablesSelection();
        }
      }

      return true;
    };

    /**
     * Changes the selected cell into a header cell or vice versa.
     */
    publicObj.toggleHeaderCell = function () {
      var $replacer;
      var $selectedTds = _globals.$element.find('.ste_active');

      if ($selectedTds.length === 0) {
        return;
      }

      //Determine what to replace with what.
      $selectedTds.each(function () {
        var $cell = $(this);

        if ($cell[0].nodeName.toLowerCase() === 'td') {
          $replacer = $('<th />');
        }
        else {
          $replacer = $('<td />');
        }

        // Make sure we copy all relevant data
        var attributes = $cell.prop("attributes");
        $.each(attributes, function () {
          $replacer.attr(this.name, this.value);
        });

        $replacer.data($cell.data());

        // Replace cell.
        $replacer.append($cell.html());
        $cell.replaceWith($replacer);
      });

      // Reset table selectability.
      makeTablesSelectable(true);

      if ($replacer) {
        setCaret($replacer[0]);
      }
    };

    /**
     * Removes the currently selected row.
     *
     * @param {string} mode
     *   'preview', 'previewReset', 'remove' (default)
     */
    publicObj.removeRow = function (mode) {
      mode = typeof mode === 'undefined' ? 'remove' : mode;
      var $activeTable = getActiveTable();
      var $activeCell = publicObj.getSelectedCells();
      var $activeRow = $activeCell.closest('tr');
      if ($activeTable.length === 0 || $activeCell.length !== 1) {
        return false;
      }
      var activeCellInfo = getCellInfo($activeCell);
      var rowCellsInfo = getCellRowCellInfo($activeCell);

      $activeTable.find('td,th').removeClass('ste_highlight');
      if (mode === 'preview') {
        $.each(rowCellsInfo, function (i) {
          if (rowCellsInfo[i].rowspan === 1) {
            rowCellsInfo[i].cell.addClass('ste_highlight');
          }
        });
      }
      else if (mode === 'remove') {
        $.each(rowCellsInfo, function (i) {
          if (rowCellsInfo[i].rowspan === 1) {
            rowCellsInfo[i].cell.remove();
          }
          else if (rowCellsInfo[i].next_row === activeCellInfo.next_row) {
            rowCellsInfo[i].cell.attr('rowspan', rowCellsInfo[i].rowspan - 1);
          }
        });

        // Clean up empty rows and select cell in next row.
        var $selectedRow = $activeRow;
        var $selectedCell, $nextRow;

        while ($selectedRow.length === 1) {
          $nextRow = $selectedRow.next();
          if ($selectedRow.children().length === 0) {
            $selectedRow.remove();
          }
          else {
            $selectedCell = $selectedRow.find('td').eq(activeCellInfo.col);
            if ($selectedCell.length === 1) {
              // Simulate user clicking on cell in next row.
              $selectedCell.trigger('selectCell.makeTablesSelectable', true);
              break;
            }
          }
          $selectedRow = $nextRow;
        }

        removeTableIfEmpty();
      }

      return true;
    };


    /**
     * Adds a column to the active table
     *
     * @param {boolean} onlyPreview
     *
     * @param {booelan} _onlyForActiveCell

     *   Used internally. If true, the active cell is split.
     */
    publicObj.addColumn = function (onlyPreview, _onlyForActiveCell) {
      onlyPreview = typeof onlyPreview === 'undefined' ? false : onlyPreview;
      _onlyForActiveCell = typeof _onlyForActiveCell === 'undefined' ? false : _onlyForActiveCell;

      var $activeCell = publicObj.getSelectedCells();

      if ($activeCell.length !== 1) {
        return false;
      }

      var activeCellInfo = getCellInfo($activeCell);
      var colCellsInfo = getCellColumnCellInfo($activeCell, _onlyForActiveCell);

      if (!onlyPreview) {
        // Sometimes all cells in the column have the same col value and the
        // same colspan value > 1. In those cases, set the colspan value of
        // those cells to 1.
        var allCellsSimilar = true;
        $.each(colCellsInfo, function (i) {
          if (i === 0) {
            return;
          }
          var cellInfo = colCellsInfo[i];
          var prevCellInfo = colCellsInfo[i - 1];
          var isSimilar = (cellInfo.col === prevCellInfo.col && cellInfo.next_col === prevCellInfo.next_col);

          allCellsSimilar = (allCellsSimilar && isSimilar) ? true : false;
        });

        if (allCellsSimilar) {
          $.each(colCellsInfo, function (i) {
            colCellsInfo[i].cell.attr('colspan', 1);
          });
        }

        // Add the column or split the cell.
        $.each(colCellsInfo, function (i) {
          var cellInfo = colCellsInfo[i];
          var $cell = cellInfo.cell;
          var nodeName;
          var $newCell;
          var cellIsActiveCell = ($cell[0] === $activeCell[0]);
          var cellIsSimilarToActiveCell = (cellInfo.col === activeCellInfo.col && cellInfo.next_col === activeCellInfo.next_col);

          if (_onlyForActiveCell === false) {
            // Add column.
            if (cellInfo.colspan > 1 && cellIsSimilarToActiveCell === false) {
              cellInfo.cell.attr('colspan', cellInfo.colspan + 1);
            }
            else {
              nodeName = cellInfo.cell[0].nodeName.toLowerCase();
              $newCell = $('<' + nodeName + '>\u200B</' + nodeName + '>');

              if (cellInfo.rowspan > 1) {
                $newCell.attr('rowspan', cellInfo.rowspan);
              }
              cellInfo.cell.after($newCell);
            }
          }
          else {
            // Split selected cell.
            if (cellIsActiveCell) {
              nodeName = cellInfo.cell[0].nodeName.toLowerCase();
              $newCell = $('<' + nodeName + '>\u200B</' + nodeName + '>');

              if (cellInfo.rowspan > 1) {
                $newCell.attr('rowspan', cellInfo.rowspan);
              }
              cellInfo.cell.after($newCell);
            }
            else {
              cellInfo.cell.attr('colspan', cellInfo.colspan + 1);
            }
          }
        });

        makeTablesSelectable(true);
      }

      return true;
    };

    /**
     * Returns the selected cells.
     *
     * @returns {object}
     *   jQuery collection.
     */
    publicObj.getSelectedCells = function () {
      var $activeTable = getActiveTable();
      if ($activeTable) {
        return $activeTable.find('.ste_active');
      }

      return $([]);
    };

    /**
     * Adds a column to the active table
     *
     * @param {string} mode
     *   'remove' (default), 'preview', 'uncheck'. The latter two states are used
     *     for highlighting.
     */
    publicObj.removeColumn = function (mode) {
      mode = typeof mode === 'undefined' ? 'remove' : mode;
      var $activeTable = getActiveTable();
      var $activeCell = publicObj.getSelectedCells();
      var $nextCell = $activeCell.next();

      if ($activeTable.length === 0 || $activeCell.length !== 1) {
        return false;
      }
      var colCellsInfo = getCellColumnCellInfo($activeCell);

      $activeTable.find('td,th').removeClass('ste_highlight');
      if (mode === 'preview') {
        $.each(colCellsInfo, function (i) {
          if (colCellsInfo[i].colspan === 1) {
            colCellsInfo[i].cell.addClass('ste_highlight');
          }
        });
      }
      else if (mode === 'remove') {
        $.each(colCellsInfo, function (i) {
          if (colCellsInfo[i].colspan === 1) {
            colCellsInfo[i].cell.remove();
          }
          else {
            colCellsInfo[i].cell.attr('colspan', colCellsInfo[i].colspan - 1);
          }
        });

        // Select next cell
        if ($nextCell.length === 1) {
          $nextCell.trigger('selectCell.makeTablesSelectable', true);
        }
      }
      removeTableIfEmpty();

      return true;
    };

    /**
     * Returns whether cursor is inside table.
     *
     * @returns {Boolean}
     */
    publicObj.inTable = function () {
      var returnValue = getActiveTable(true).length !== 0;

      if (returnValue !== true) {
        // Reset selection if not in table.
        resetTablesSelection();
      }

      return returnValue;
    };

    /**
     * Cleans up the HTML, as various function might have altered it. Call this
     * function before further processing of the HTML.
     *
     * @returns {undefined}
     */
    publicObj.cleanHTML = function () {
      _globals.$element.find('.ste').removeClass('ste');
      _globals.$element.find('.ste_active,.ste_highlight').removeClass('ste_active').removeClass('ste_highlight');
      _globals.$element.find('.ste_copy').remove();
      _globals.$element.find('table, td, th').css('user-select', '');

      // Remove obsolete attributes.
      _globals.$element.find('table, td, th').each(function () {
        var $this = $(this);

        if ($this.attr('style') === '') {
          $this.removeAttr('style');
        }

        if ($this.attr('class') === '') {
          $this.removeAttr('class');
        }
      });
    };

    /**
     * Simplifies and cleans the HTML, so that this plugin understands it.
     *
     * @todo Add tbody/thead support to this plugin.
     *
     * @returns {undefined}
     */
    function preClean() {
      // Remove tbody/thead
      _globals.$element.find('table').each(function () {
        var $table = $(this);
        $table.find('thead, tbody').each(function () {
          var $container = $(this);
          var $children = $container.children();
          $container.before($children);
          $container.remove();
        });
      });
    }

    /**
     * Makes table navigationable using the 'tab' key.
     *
     * @param {boolean} reset
     */
    function makeTablesNavigationable(reset) {
      var $document = $(document);
      if (reset === true) {
        $document.data('makeTablesNavigationable', false);
      }

      // Only init once.
      if ($document.data('makeTablesNavigationable') === true) {
        return;
      }
      else {
        $document.data('makeTablesNavigationable', true);
      }

      /**
       * Handler function
       */
      var onKeyPressSetCursor = function (e) {
        var arrowPressed = false;
        var $table = getActiveTable();

        if ($table.length === 0) {
          return true;
        }

        var $activeCell = publicObj.getSelectedCells();
        var $nextCell;
        if ($activeCell.length !== 1) {
          return;
        }

        var activeCellInfo = getCellInfo($activeCell);

        if (e.keyCode === 37) { //move left or wrap
          $nextCell = getCell($table, activeCellInfo.col - 1, activeCellInfo.row);
          arrowPressed = true;
        }
        if (e.keyCode === 38) { // move up
          $nextCell = getCell($table, activeCellInfo.col, activeCellInfo.row - 1);
          arrowPressed = true;
        }
        if (e.keyCode === 39) { // move right or wrap
          $nextCell = getCell($table, activeCellInfo.next_col, activeCellInfo.row);
          arrowPressed = true;
        }
        if (e.keyCode === 40) { // move down
          $nextCell = getCell($table, activeCellInfo.col, activeCellInfo.next_row);
          arrowPressed = true;
        }

        // Set cursor in active cell.
        if (typeof $nextCell !== 'undefined' && $nextCell.length === 1) {
          resetTablesSelection();
          $activeCell.removeClass('ste_active');
          $nextCell.addClass('ste_active');
          $nextCell.trigger("focus");
          setCaret($nextCell[0]);
        }

        // Make sure this event doesn't bubble if cursor pressed.
        if (arrowPressed === true) {
          e.preventDefault();
          return false;
        }

        return true;
      };

      // Set the handler to the document.
      $document.off('keydown.makeTablesNavigationable').on('keydown.makeTablesNavigationable', function (e) {
        return onKeyPressSetCursor(e);
      });


    }

    /**
     * Enables copy button added to tables (as we had to disable native text
     * selection inside tables).
     *
     */
    function makeTablesCopyable() {
      _globals.$element.find('table').each(function () {
        var $table = jQuery(this);

        if ($table.data('copybutton')) {
          return;
        }

        // Add copy button to table 'data'.
        $table.addClass('ste');
        var $copyButton = jQuery('<div contenteditable="false" class="ste_copy"><i class="fa fa-clipboard"></i></div>');
        $copyButton.attr('title', Drupal.t('Copy table to clipboard'));
        $table.data('copybutton', $copyButton);

        // Enable copy button.
        $copyButton.on('click', function () {
          var $button = $(this);
          var $table = $button.closest('table');

          // Do some cleaning.
          $table.css('user-select', '');
          $table.find('.ste_active').removeClass('ste_active');
          $table.css('position', '');
          $button.detach();
          $button.data('mouseover', false);

          // Copyt data to clipboard.
          copyElementToClipboard($table[0]);

          // Reset css.
          $table.css('position', 'relative');
          $table.css('user-select', 'none');
        });

        // Set flag so button wont be hidden, as cursor will leave table.
        $copyButton.on('mouseover', function () {
          $(this).data('mouseover', true);
        }).on('mouseleave', function () {
          $(this).data('mouseover', false);
        });

        // Show/hide button when user is inside/outside table or
        // inside/outside button.
        $table.on('mouseover.makeTablesCopyable', function () {
          var $copyButton = $(this).data('copybutton');
          $table.append($copyButton);
        }).on('mouseleave.makeTablesCopyable', function () {
          var $copyButton = $(this).data('copybutton');
          if ($copyButton.data('mouseover') !== true) {
            $copyButton.detach();
          }
        });
      });
    }

    /**
     * Helper. Copies DOM element to clipboard.
     *
     * @param {object} element
     *   A DOM element.
     */
    function copyElementToClipboard(element) {
      window.getSelection().removeAllRanges();
      let range = document.createRange();
      range.selectNode(typeof element === 'string' ? document.getElementById(element) : element);
      window.getSelection().addRange(range);
      document.execCommand('copy');
      window.getSelection().removeAllRanges();
    }

    /**
     * Resets the selected cells.
     *
     * @param {obj} $skipTable
     *   Optional. If provided, the cells of this table are NOT reset.
     */
    function resetTablesSelection($skipTable) {
      if (!$skipTable) {
        _globals.$element.find('th,td').removeClass('ste_active');
      }
      else {
        _globals.$element.find('table').not($skipTable).find('th,td').removeClass('ste_active');
      }
    }

    /**
     *Disables cross-browser selection of table cells.
     */
    function destroyTablesSelectable() {
      resetTablesSelection();

      _globals.$element.find('table').each(function () {
        var $table = jQuery(this);
        $table.data('makeTablesSelectable', false);
        $table.find('th,td').off('click.makeTablesSelectable');
      });
    }

    /**
     * Enables cross-browser (fingers crossed!) selection of table cells. The
     *  user should press the Ctrl key to select cells.
     *
     *  @param {booelan} force
     *    Forces reset of events.
     */
    function makeTablesSelectable(force) {
      if (force === true) {
        destroyTablesSelectable();
      }
      var $activeNode = getCaretNode();

      _globals.$element.find('table').each(function () {
        var $table = jQuery(this);

        // Only init once.
        if ($table.data('makeTablesSelectable') === true) {
          return;
        }

        $table.data('makeTablesSelectable', true);
        $table.data('selecting', false);

        $table.off('mouseleave.makeTablesSelectable').on('mouseleave.makeTablesSelectable', function (e) {
          $(this).removeData('firstCellInfo');
        });

        // Enable selecting of cells when holding mouse button down and moving over cells.
        // Also add custom selectCell event, to allow for triggering without event bubbling.
        var $cells = $table.find('th,td');
        $cells.off('mouseup.makeTablesSelectable selectCell.makeTablesSelectable').on('mouseup.makeTablesSelectable selectCell.makeTablesSelectable', function (e, ctrlKey) {
          if (ctrlKey) {
            e.ctrlKey = ctrlKey;
          }
          if (e.shiftKey) {
            e.preventDefault();
            return false;
          }
          if (e.ctrlKey) {
            // Assume user is adding to current selection while dragging.
            if (publicObj.getSelectedCells().length > 1) {
              e.dontToggle = true;
            }else if ($(this).hasClass('ste_active') && this === $table.data('firstCellInfo').cell[0]) {
              // Assume user clicked this cell while pressing Ctrl
              e.dontToggle = false;
            }
            _makeTablesCellSelectHandler(e);
          }

          // Remove first cell info.
          $(this).closest('table').removeData('firstCellInfo');
        });

        $cells.off('mouseover.makeTablesSelectable mousedown.makeTablesSelectable').on('mouseover.makeTablesSelectable mousedown.makeTablesSelectable', function (e) {
          var $table;
          if (e.shiftKey) {
            e.preventDefault();
            return false;
          }

          if (e.buttons === 1) {
            $table = $(this).closest('table');

            // Save first cell's info.
            if (!$table.data('firstCellInfo')) {
              $table.data('firstCellInfo', getCellInfo($(this)));
            }

            // Make sure we always select (not unselect) sales while holding down mouse button.
            e.dontToggle = true;

            _makeTablesCellSelectHandler(e);
          }
        });

        $cells.off('mousedown.makeTablesSelectableCaret').on('mousedown.makeTablesSelectableCaret', function (e) {
          setCaret(this);
        });

        $cells.off('mouseleave.makeTablesSelectable').on('mouseleave.makeTablesSelectable', function (e) {
          if (e.shiftKey) {
            e.preventDefault();
            return false;
          }
          jQuery(this).css('user-select', '');
        });


        // This plugin is for the contenteditable, not for the table. So the first
        // time the end user clicks somewhere in the table, we should ensure that
        // this event is also taken into account.
        if ($table[0] === getActiveTable()[0] && publicObj.getSelectedCells().length === 0 && $activeNode[0] && ($activeNode[0].nodeName.toLowerCase() === 'td' || $activeNode[0].nodeName.toLowerCase() === 'th')) {
          _makeTablesCellSelectHandler({"target": $activeNode[0]});
        }
      });
    }

    /**
     * Handler for select events on selectable cells.
     * @param {type} e
     * @returns {undefined}
     */
    function _makeTablesCellSelectHandler(e) {
      var row, col, $cell;
      var $current_cell = $(e.target);
      var $table = $current_cell.closest('table');

      var firstCellInfo = $table.data('firstCellInfo');
      if ($current_cell[0].nodeName.toLowerCase() !== 'td' && $current_cell[0].nodeName.toLowerCase() !== 'th') {
        return;
      }

      var currentCellInfo = getCellInfo($current_cell);

      if (!e.ctrlKey) {
        $table.find('th,td').removeClass('ste_active');
      }

      // Reset other tables.
      if (!firstCellInfo || $current_cell[0] === firstCellInfo.cell[0]) {
        resetTablesSelection($table);
      }

      if (!firstCellInfo) {
        // Mimic first cell is current cell.
        firstCellInfo = {"row": currentCellInfo.row, "col": currentCellInfo.col};
      }

      var top_row = Math.min(firstCellInfo.row, currentCellInfo.row);
      var bottom_row = Math.max(firstCellInfo.row, currentCellInfo.row);
      var top_col = Math.min(firstCellInfo.col, currentCellInfo.col);
      var bottom_col = Math.max(firstCellInfo.col, currentCellInfo.col);

      // Select cells.
      for (row = top_row; row <= bottom_row; row++) {
        for (col = top_col; col <= bottom_col; col++) {
          $cell = getCell($table, col, row);

          // Toggle class.
          if (e.dontToggle === true) {
            $cell.addClass('ste_active');
          }
          else {
            $cell.toggleClass('ste_active');
          }
        }
      }
    }

    /**
     * Get info of a table cell.
     *
     * @param {obj} $cell
     *  The jQuerified DOM object.
     *
     * @param {boolean} skipRecalc
     *   If set to 'true', recalculateTableCellInfo() will not be called. This
     *    function recalculates cell positions. If getCellInfo is called in a
     *    loop, increase performance by manually calling
     *    recalculateTableCellInfo() first and then setting skipRecalc to 'true'.
     *
     * @returns {obj}
     *   {
     *     "col": (int) column index,
     *     "row": (int) row index,
     *     "colspan": value of colspan attribute.
     *     "rowspan": value of rowspan attribute.
     *     "next_col": the column index of the next column
     *     "next_row":  the column index of the next row
     *     "cell": $cell,
     *   }
     */
    function getCellInfo($cell, skipRecalc) {
      var doRecalc = typeof skipRecalc === 'undefined' ? true : !skipRecalc;
      var $table;
      var pos;
      var colspan, rowspan;

      if ($cell) {
        $table = $cell.closest("table, thead, tbody, tfoot");

        if (doRecalc) {
          recalculateTableCellInfo($table);
        }

        pos = $cell.data("cellPos");
        colspan = getNumericAttrValue($cell, 'colspan') || 1;
        rowspan = getNumericAttrValue($cell, 'rowspan') || 1;

        return {
          "col": pos.left,
          "row": pos.top,
          "colspan": colspan,
          "rowspan": rowspan,
          "next_col": pos.left + colspan,
          "next_row": pos.top + rowspan,
          "cell": $cell
        };
      }
      else {
        return {};
      }
    }

    /**
     * Returns a cell based on its position in the table.
     *
     * @param {object} $table
     *   The jQuerified table object.
     *
     * @param {integer} col
     *
     * @param {integer} row
     *
     * @returns {object}
     *   The jQuerified cell object.
     */
    function getCell($table, col, row) {
      var $returnCell = jQuery("");

      // Recalculate table.
      recalculateTableCellInfo($table);

      // Walk through all cells.
      var $cells = $table.find('th,td');
      $cells.each(function () {
        if ($returnCell.length > 0) {
          return;
        }
        var cellInfo = getCellInfo($(this), true);
        var sameRow = (row === cellInfo.row || (row > cellInfo.row && row < cellInfo.next_row));
        if (!sameRow) {
          return;
        }
        var sameColumn = (col === cellInfo.col || (col > cellInfo.col && col < cellInfo.next_col));
        if (sameColumn) {
          // Set return cell.
          $returnCell = cellInfo.cell;
        }
      });

      // Return cell.
      return $returnCell;
    }

    /**
     * Returns the cell info for all cells in the same column as the provided
     *  cell.
     *
     * @param {object} $activeCell
     *   The cell.
     *
     * @param {boolean} leftAlign
     *   Default: false. If true, the function returns cells having the same
     *   left border. Default: false, so right border cells.
     *
     * @returns {array}
     *   Array of objects returned by function getCellInfo.
     */
    function getCellColumnCellInfo($activeCell, leftAlign) {
      var leftAlign = typeof leftAlign === 'undefined' ? false : leftAlign;
      var $activeTable = getActiveTable();
      var activeCellInfo = getCellInfo($activeCell);
      var cellsInfo = [];

      var activeCellLeft = activeCellInfo.col;
      var activeCellRight = activeCellInfo.next_col;

      recalculateTableCellInfo($activeTable);
      $activeTable.find('th,td').each(function () {
        var $cell = $(this);
        var isWithinRange = false;
        var cellInfo = getCellInfo($cell, true);
        var cellLeft = cellInfo.col;
        var cellRight = cellInfo.next_col;

        if (activeCellInfo.colspan === 1 || leftAlign === true) {
          // Active cell has no colspan, find cells which share the same 'left'
          // boundary.
          if (cellLeft === activeCellLeft) {
            // Same column.
            isWithinRange = true;
          }
          else if (cellLeft < activeCellLeft && (cellRight >= activeCellRight || cellRight > activeCellLeft)) {
            isWithinRange = true;
          }
        }
        else {
          // Active cell has colspan, find cells which share the same boundary
          // on the right.
          if (cellRight === activeCellRight) {
            isWithinRange = true;
          }
          else if (cellLeft < activeCellRight && cellRight >= activeCellRight) {
            isWithinRange = true;
          }
        }

        if (isWithinRange) {
          cellsInfo.push(cellInfo);
        }
      });

      return cellsInfo;
    }

    /**
     * Returns the cell info for all cells in the same column as the provided
     *  cell.
     *
     * @param {object} $activeCell
     *   The cell.
     *
     * @param {boolean} topAlign
     *   Default: false. If true, the function returns cells having the same
     *   top border. Default: false, so bottom border cells.
     *
     * @returns {array}
     *   Array of objects returned by function getCellInfo.
     */
    function getCellRowCellInfo($activeCell, topAlign) {
      topAlign = typeof topAlign === 'undefined' ? false : topAlign;
      var $activeTable = getActiveTable();
      var activeCellInfo = getCellInfo($activeCell);
      var cellsInfo = [];

      var activeCellTop = activeCellInfo.row;
      var activeCellBottom = activeCellInfo.next_row;

      recalculateTableCellInfo($activeTable);
      $activeTable.find('th,td').each(function () {
        var $cell = $(this);
        var isWithinRange = false;

        var cellInfo = getCellInfo($cell, true);
        var cellTop = cellInfo.row;
        var cellBottom = cellInfo.next_row;

        if (activeCellInfo.rowspan === 1 || topAlign === true) {
          // Active cell has no rowspan, find cells which share the same 'top'
          // boundary.
          if (cellTop === activeCellTop) {
            // Same column.
            isWithinRange = true;
          }
          else if (cellTop < activeCellTop && (cellBottom >= activeCellBottom || cellBottom > activeCellTop)) {
            isWithinRange = true;
          }
        }
        else {
          // Active cell has rowspan, find cells which share the same boundary
          // at the bottom.
          if (cellBottom === activeCellBottom) {
            isWithinRange = true;
          }
          else if (cellTop < activeCellBottom && cellBottom >= activeCellBottom) {
            isWithinRange = true;
          }
        }

        if (isWithinRange) {
          cellsInfo.push(cellInfo);
        }
      });

      return cellsInfo;
    }

    /**
     * For each table cell, calculates the top/left position (row index, col
     * index, corrected for colspan / rowspan).
     * @credit nrodic https://stackoverflow.com/questions/13407348/table-cellindex-and-rowindex-with-colspan-rowspan/13426773
     *
     * @param {type} $table
     *
     * @returns {void}
     *   The {top: int, left: int} values are set using jQuery data function
     *   for each cell: $(my-cell-selector).data('cellPos');
     */
    function recalculateTableCellInfo($table) {
      if ($table.data('st.html') === $table.html()) {
        return;
      }

      var m = [];
      $table.children("tr").each(function (y, row) {
        $(row).children("td, th").each(function (x, cell) {
          var $cell = $(cell),
            cspan = $cell.attr("colspan") | 0,
            rspan = $cell.attr("rowspan") | 0,
            tx, ty;
          cspan = cspan ? cspan : 1;
          rspan = rspan ? rspan : 1;
          for (; m[y] && m[y][x]; ++x)
            ;  //skip already occupied cells in current row
          for (tx = x; tx < x + cspan; ++tx) {  //mark matrix elements occupied by current cell with true
            for (ty = y; ty < y + rspan; ++ty) {
              if (!m[ty]) {  //fill missing rows
                m[ty] = [];
              }
              m[ty][tx] = true;
            }
          }
          var pos = {top: y, left: x};
          $cell.data("cellPos", pos);
        });
      });

      $table.data('st.html', $table.html());
    }
    ;


    /**
     * Adds an empty table.
     *
     * @param {object} dimensions
     *   {"cols": int, "rows": int}.
     *
     * @returns {object}
     *   The table DOM node
     */
    function addEmptyTable(dimensions) {
      dimensions = typeof dimensions === 'undefined' ? {"rows": 2, "cols": 2} : dimensions;
      var $activeNode = getCaretNode();
      var $row;
      var c = 0;
      var r = 0;
      var $returnNode = $('<table></table>');
      for (r = 0; r < dimensions.rows; r++) {
        $row = $('<tr></tr>');
        for (c = 0; c < dimensions.cols; c++) {
          $row.append('<td>\u200B</td>');
        }
        $returnNode.append($row);
      }

      var returnNode = $returnNode[0];

      if ($activeNode[0] === element) {
        var sel = window.getSelection();
        var range = sel.getRangeAt(0);
        range.insertNode(document.createTextNode("\u200B"));
        range.insertNode(returnNode);

      }
      else {
        // Add table after active node.
        if (isBlockLevelElement($activeNode[0])) {
          $activeNode.after(returnNode);
        }
        else {
          // Fixes IE bug sometimes inserting the table inside an inline element.
          $activeNode.parents().filter(function () {
            return $(this).css("display") === "block";
          }).first().after(returnNode);
        }
      }

      // Add zero-width space for selection in contenteditable.
      $(returnNode).after("\u200B");

      makeTablesSelectable();
      return returnNode;
    }

    /**
     * Returns numberic attribute property (or 0 if its is not a numeric value).
     *
     * @param {DOM obj} el     *
     * @param {string} attName
     *
     * @returns {integer}
     */
    function getNumericAttrValue(el, attName) {
      var returnValue = $(el).attr(attName);

      if (isNaN(returnValue)) {
        returnValue = 0;
      }

      return parseInt(returnValue, 10);
    }

    /**
     * Returns whether DOM element is block-level or inline.
     *
     * @param {object} domEl
     * @returns {Boolean}
     */
    function isBlockLevelElement(domEl) {
      var displayStyle;
      if (domEl) { // Only if the element exists
        if (window.getComputedStyle) {
          displayStyle = window.getComputedStyle(domEl, null).getPropertyValue('display');
        }
        else {
          displayStyle = domEl.currentStyle.display;
        }
      }
      return displayStyle === 'block';
    }

    /**
     * Removes an empty table.
     */
    function removeTableIfEmpty() {
      var $activeTable = getActiveTable();
      if ($activeTable.length === 1 && (($activeTable.find('td').length === 0 && $activeTable.find('th').length === 0) || $activeTable.find('tr').length === 0)) {
        $activeTable.remove();
      }
    }

    /**
     * Returns the active table in a contenteditable field.
     *
     * @param {boolean} forceDS
     *  Force obtaining the active table via the document.selection method.
     *  Default: false. Otherwise the active cell is found and its parent
     *  table is returned.
     *
     * @return {object} A jQuery object.
     **/
    function getActiveTable(forceDS) {
      forceDS = typeof forceDS === 'undefined' ? false : true;

      // Try via clicked cell.
      if (forceDS === false) {
        var $cell = _globals.$element.find('.ste_active');
        if ($cell.length > 0) {
          return $cell.eq(0).closest('table');
        }
      }

      // Try via document.selection.
      var $activeNode = getCaretNode();
      if ($activeNode.length > 0) {
        if ($activeNode[0].nodeName.toLowerCase() !== 'table') {
          return $activeNode.closest("table", _globals.$element[0]);
        }
        else {
          return $activeNode;
        }
      }

      return jQuery("");
    }

    /**
     * Returns the node containing the caret.
     *
     * $return {object}  A JQuery object.
     **/
    function getCaretNode() {
      var selection;
      var returnNode;
      if (window.getSelection) {
        selection = window.getSelection();
      }
      else if (document.selection && document.selection.type !== "Control") {
        selection = document.selection;
      }
      if (selection.anchorNode && selection.anchorNode.nodeType === 3) {
        returnNode = selection.anchorNode.parentNode;
      }
      else {
        returnNode = selection.anchorNode;
      }

      return jQuery(returnNode);
    }

    /**
     * Sets the caret inside a DOM element
     * @param {object} domEl
     *   A DOM object.
     * @credits
     *   https://stackoverflow.com/questions/6249095/how-to-set-caretcursor-position-in-contenteditable-element-div?answertab=active#tab-top
     * @returns {undefined}
     */
    function setCaret(domEl) {
      if (domEl) {
        var range = document.createRange();
        var sel = window.getSelection();
        range.setStart(domEl, 0);
        range.collapse(true);
        sel.removeAllRanges();
        sel.addRange(range);
      }
    }

    /* initialize simpleTableEdit
     */
    if (_cdnFilesToBeLoaded.length === 0) {
      _helper.doInit();
    }
  };

  $.fn.simpleTableEdit = function (settings) {
    return this.each(function () {
      if (undefined === $(this).data("simpleTableEdit")) {
        var plugin = new $.simpleTableEdit(this, settings);
        $(this).data("simpleTableEdit", plugin);
      }
    });
  };
})(jQuery);
