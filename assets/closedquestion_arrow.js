/**
 * @requires  closedquestion_arrow_helper.js
 */
(function ($) {
  "use strict";

  /**
   * Add Drupal behavior: Connect the cqArrowQuestion to the
   * Drupal form element
   */
  Drupal.behaviors.closedQuestionArrow = {
    "attach": function (context) {
      var questionId;
      var settings = Drupal.settings.closedQuestion.cr;

      for (questionId in settings) {
        /* create cqArrowQuestion objects */
        var $answerContainer = $("#" + questionId + "answerContainer");
        var $image_element = $answerContainer.find('img').first();
        var $answerFormElement = $('input[name=' + questionId + 'answer]');

        if ($image_element.data('cqArrowQuestion') === undefined) {
          /* init arrow question plugin */
          var cqArrowQuestionPromise = cqArrowQuestion($image_element, settings[questionId]);

          cqArrowQuestionPromise.then(function () {
            /* set current answer when plugin is inited */
            var cqArrowQuestion = $image_element.data('cqArrowQuestion');
            cqArrowQuestion.setAnswer($answerFormElement.val());

            /* create handler to connect plugin to closed question form */
            var onUpdateAnswer = function () {
              $answerFormElement.val(cqArrowQuestion.getAnswer());
            };
            /* let question tell us when user updates answer */
            cqArrowQuestion.registerPlugin("submitFeedback", {"hooks": {
                "onAddLineToAnswer": onUpdateAnswer,
                "onRemoveLineFromAnswer": onUpdateAnswer
              }});
          });
        }
      }
    }
  };
})(jQuery);