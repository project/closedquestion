
/**
 *@file
 *Javascript functions for the Select&Order questions.
 */

/**
 * Generate the answer string from the draggables, for the question with the
 * given id and puts it in the answer form field for that question.
 *
 * @param elementName string
 *   The element name of the question to generate the answer string for.
 *
 * @return TRUE
 */
function cqCheckAnswerSO(elementName) {
  var answer = "";
  jQuery('#' + elementName + 'targets ul.cqDDList').each(function () {
    var $ulItem = jQuery(this);
    answer += $ulItem.attr("cqvalue");
    jQuery('li.cqDraggable', $ulItem).each(function () {
      answer += jQuery(this).attr("cqvalue");
    });
  });

  var answerElement = jQuery('[name="' + elementName + 'selected"]');
  answerElement.val(answer);
}

/**
 * Removes an item from the selection.
 *
 * @param item draggable
 *   The item to remove.
 */
function cqRemoveItem(item) {
  var liItem = item.parentNode;
  var ulItem = liItem.parentNode;
  ulItem.removeChild(liItem);
  var listItem = ulItem.parentNode.parentNode;
  var longid = listItem.id;
  var id = longid.substring(0, longid.length - 7);
  cqCheckAnswerSO(id);
}

/**
 * Attach the code that makes the items sortable to a Drupal behaviour.
 */
Drupal.behaviors.closedQuestionSO = {
  attach: function ($context) {
    var questionId;
    var settings = Drupal.settings.closedQuestion.so;
    var questionSettings;
    var duplicates = false;

    var overSortHandler = function (event, ui) {
      var height, width;
      if (duplicates) {
        height = ui.helper.height();
        width = ui.helper.width();
      }
      else {
        height = ui.item.height();
        width = ui.item.width();
      }

      // Show placeholder with same dimensions in target.
      jQuery(this).find('.ui-sortable-placeholder').css({
        "height": height,
        "width": width
      });
    };

    for (questionId in settings) {
      questionSettings = settings[questionId];
      duplicates = parseInt(questionSettings.duplicates, 10) === 1;
      /* Create sortable. */
      jQuery('.cqDropableList:not(.cqSort-processed)', $context).sortable({
        "connectWith": ".cqDropableList",
        "update": function (event, ui) {
          var listItem = ui.item[0].parentNode.parentNode.parentNode;
          var longid = listItem.id;
          var id = longid.substring(0, longid.length - 7);
          cqCheckAnswerSO(id);
        },
        "remove": function (event, ui) {
          var listItem = ui.item[0].parentNode.parentNode.parentNode;
          var longid = listItem.id;
          var id = longid.substring(0, longid.length - 7);
          cqCheckAnswerSO(id);
        },
        "sort": overSortHandler,
        "over": overSortHandler,
        "receive": function (event, ui) {
          let $this = jQuery(this);

          /* Re-initiate tooltips. */
          $this.find('.cqTooltipProcessed').removeClass('cqTooltipProcessed');
          cqInitTooltips($this);
        }
      });

      /* special settings for 'onlyorder' questions */
      if (parseInt(questionSettings.onlyOrder, 10) === 1) {
        // only order: move all options to target and hide source.
        jQuery('.cqDropableList:not(.cqSort-processed) li.cqDraggable', $context).each(function () {
          var $li = jQuery(this);
          var $target = jQuery('.cqDropableList');

          $target.append($li).sortable('option', 'update')(null, {"item": $li});
        });
        jQuery('.cqSource, .cqTargetTitle').hide();
      }

      /* create draggables */
      jQuery('.cqCopyList .cqDraggable:not(.cqDrag-processed)', $context).draggable({
        connectToSortable: "ul.cqDropableList",
        helper: "clone",
        zIndex: 1000,
        start: function(event, ui){
          if (ui && ui.helper) {
            // Remove mathjax stuff to prevent double rendering.
            // Should not be our concern of course.
            jQuery(ui.helper).find('script[type="math/tex"]').remove();
            jQuery(ui.helper.context).find('script[type="math/tex"]').remove();
          }
        }
      });

      /* do some css tweaks */
      jQuery(".cqDDList").css("list-style-type", "none");
      jQuery(".cqDraggable").css("background-image", "none");

      jQuery('.cqDraggable:not(.cqX-processed)', $context).addClass('cqX-processed').prepend("<a class='remove' onclick='cqRemoveItem(this)'>X</a>");

      jQuery('.cqDropableList:not(.cqSelect-processed)', $context).disableSelection();

      // set flags to prevent double inits
      jQuery('.cqDropableList:not(.cqSort-processed)', $context).addClass('cqSort-processed');
      jQuery('.cqCopyList .cqDraggable:not(.cqDrag-processed)', $context).addClass('cqDrag-processed');
      jQuery('.cqDropableList:not(.cqSelect-processed)', $context).addClass('cqSelect-processed');
    }
  }
};
