
/**
 *@file
 *Javascript functions for the Drag&Drop questions.
 */

/**
 * Attach the code that puts all draggables in the right spot, and loads
 * the dragArea to a Drupal behaviour.
 */
Drupal.behaviors.closedQuestionDD = {
  attach: function (context) {
    var settings = Drupal.settings.closedQuestion.dd;
    for (var questionId in settings) {
      jQuery(window).bind('load', function () {
        /* init */
        var qsettings = settings[questionId];
        qsettings.formElement = jQuery('[name="' + questionId + 'answer"]');
        qsettings.questionid = questionId;

        cqInitDDQuestion(qsettings);
      });
    }
  }
};


/**
 * Generate the answer string from the draggables, for the question with the
 * given id and puts it in the answer form field for that question.
 *
 * @param questionId string
 *   The question id of the question to generate the answer string for.
 *
 * @return TRUE
 */
function cqCheckAnswerDD(questionId) {
  var returnString = "";
  var qsettings = Drupal.settings.closedQuestion.dd[questionId];
  var length = qsettings.ddDraggableStartPos.length;
  var i;

  for (i = 0; i < length; i++) {
    var draggableVar = qsettings.ddDraggableStartPos[i];
    returnString += "" + draggableVar.cqvalue + "," + draggableVar.x + "," + draggableVar.y + ";";
  }
  var answerElement = jQuery('[name="' + questionId + 'answer"]');
  answerElement.val(returnString);
  return true;
}
