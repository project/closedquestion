
/**
 *@file
 *Javascript functions for Closed Questions.
 */

/**
 * Attach the code that initializes the closed question
 */
Drupal.behaviors.closedQuestion = {
  attach: function (context) {
    Drupal.settings.closedquestion = typeof Drupal.settings.closedquestion !== 'undefined' ? Drupal.settings.closedquestion : {};

    /* Turn autocomplete off for inputs */
    jQuery('#closedquestion-get-form-for input').attr('autocomplete', 'off');

    /* Turn off enter key submission of form */
    jQuery('#closedquestion-get-form-for :input').keypress(function (event) {
      if (parseInt(event.keyCode, 10) === 13) {
        event.preventDefault();
      }
    });

    /* Prevent accidental submitting of the form (and thus: page reloading)
     * in case AJAX is enabled.
     */
    if (Drupal.settings && Drupal.settings.ajax && Drupal.settings.ajax['edit-submit'] && Drupal.ajax['edit-submit'].callback === 'closedquestion_submit_answer_js') {
      // Create a setting so we can suppress this behavior.
      Drupal.settings.closedquestion.preventFormSubmit = true;

      jQuery('#closedquestion-get-form-for').off('submit.closedquestion').on('submit.closedquestion', function () {
        if (Drupal.settings.closedquestion.preventFormSubmit === true) {
          return false;
        }
      });
    }

    /* Put in-line feedback in the right spot. */
    cqMoveFeedback(context);

    /* Add prompt to reset button. */
    if (context === document) {
      jQuery('#edit-reset', context).off('click.closedquestion').on('click.closedquestion', function (e) {
        if (!confirm(Drupal.t('Are you sure you want to reset your answer?'))) {
          e.preventDefault();
        }

        // Make sure the form can be submitted
        Drupal.settings.closedquestion.preventFormSubmit = false;
      });
    }
  }
};

/**
 * Function that moves feedback from the general feedback area to the correct
 * place in the question.
 *
 * It clears out all existing feedback first, so only the new feedback is
 * visible.
 *
 * @param {object} context
 *   The parent DOM element.
 */
function cqMoveFeedback(context) {
  var feedback = jQuery('[id^="cq-feedback-wrapper"]', context);
  if (feedback.length > 0) {
    jQuery(".cqFbBlock").empty();
    jQuery(".cqFbItem", context).filter("[class*=block-]").each(function (index, Element) {
      var fbItem = jQuery(this);
      var parent = fbItem.parent();
      var fieldSet = parent.parent();
      var classString = fbItem.attr("class");
      var pos1 = classString.indexOf("block-");
      var pos2 = classString.indexOf(" ", pos1 + 6);
      if (pos2 < 0) {
        pos2 = classString.length;
      }
      var targetId = classString.substr(pos1 + 6, pos2 - pos1);
      var targetBlock = jQuery(".cqFbBlock.cqFb-" + targetId + "");
      targetBlock.append(fbItem);
      parent.remove();
      var fieldSetChildren = fieldSet.children();
      // If there are no more children in the fieldset, we can hide it.
      if (fieldSetChildren.length === 0) {
        fieldSet.parent().hide();
      }
    });
  }
}

/**
 * Add tooltips to targets
 * @param {object} context
 *   The parent DOM element. Default: body.
 * @returns {undefined}
 */
function cqInitTooltips(context) {
  var $ = jQuery;
  var $context = context === undefined ? $('body') : $(context);
  var $tooltipTargets = $('.cqTooltipTarget', $context).not('.cqTooltipProcessed');

  if ($.ui && $.ui.tooltip) {
    $tooltipTargets.tooltip({
      "content": function () {
        return decodeURIComponent($(this).attr('title'));
      },
      "tooltipClass": "cqTooltip",
      "track": true,
      "position": {
        "collision": "flipfit"
      }
    });
  }
  else {
    /* no jquery ui tooltip: make title attribute readable */
    $tooltipTargets.each(function () {
      var $this = $(this);
      var title = decodeURIComponent($this.attr('title'));
      $this.attr('title', $('<div />').html(title).text());
    });
  }

  $tooltipTargets.addClass('cqTooltipProcessed');
}

(function ($) {
  /**
   * Triggers an AJAX event.
   *
   * @param {type} name
   *   The name of the event.
   * @param {type} data
   *   The data
   */
  $.fn.cqTriggerAjaxEvent = function (name, data) {
    function _trigger(name, data) {
      name = name.charAt(0).toUpperCase() + name.slice(1);
      var $eventTriggerElement = $('body');
      $eventTriggerElement.trigger('closedquestion.on' + name, data);
    }

//    switch (name) {
//      case 'updateFeedback':
//        var feedbackHTML = data.feedback;
//        var answer = data.answer;
//        var correct = $(feedbackHTML).find('.cq_error').length === 0;
//        data = {"answer": answer, "feedback": data, "correct": correct};
//        break;
//    }

    // Tell the world.
    _trigger(name, data);
  };
})(jQuery);

/* Create tooltips. */
(function ($) {
  $(document).ready(function () {
    cqInitTooltips();
  });
}(jQuery));


/**
 * Console.log Fallback for Internet Explorer.
 */
(function () {
  var method;
  var noop = function () {
  };
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
}());